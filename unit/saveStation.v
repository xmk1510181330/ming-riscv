/**
 ** 保留站的实现逻辑
 ** 标记一下，这里应该准备三个就绪队列，分别对应alu，乘法和除法
 ** 每次时钟上升沿，从队列中取出一个就绪块，送入到执行单元，运算得到结果
**/

module saveStation (
    input wire clk,
    input wire save_en_1,                  //保存到保留站的使能信号
    input wire save_en_2,
    input wire [11:0] pc_1, pc_2,
    input wire [2:0] work_mode_1,           //运算模式--普通整数运算 or 整数乘除法 or 浮点
    input wire [2:0] work_mode_2,
    input wire [3:0] alu_ctrl_1_sch,           //运算器的控制信号
    input wire [3:0] alu_ctrl_2_sch,
    input wire [3:0] complex_ctrl_1_sch,       //乘除法的控制信号
    input wire [3:0] complex_ctrl_2_sch,    
    input wire [31:0] imm_1, imm_2,        //立即数的输入  
    input wire rs_1_ready,                 //寄存器是否准备就绪的标记
    input wire rt_1_ready,             
    input wire rs_2_ready,             
    input wire rt_2_ready,             
    input wire [4:0] rs_1_relation,    //和rs寄存器关联的ROB号
    input wire [4:0] rt_1_relation,    //和rt寄存器关联的ROB号
    input wire [4:0] rd_1_relation,    //和rd寄存器关联的ROB号
    input wire [4:0] rs_2_relation,    //和rs寄存器关联的ROB号
    input wire [4:0] rt_2_relation,    //和rt寄存器关联的ROB号
    input wire [4:0] rd_2_relation,    //和rd寄存器关联的ROB号
    input wire rd_1_flag, rd_2_flag,
    input wire [31:0] rs_1_value,      //寄存器的值
    input wire [31:0] rt_1_value,      
    input wire [31:0] rs_2_value,      //寄存器的值
    input wire [31:0] rt_2_value,
    input wire [2:0] dm_rd_ctrl_1, dm_rd_ctrl_2,  //RAM读取控制信号
    input wire [1:0] dm_wr_ctrl_1, dm_wr_ctrl_2,  //RAM写入控制信号
    input wire alu_commit_en_1, alu_commit_en_2, mul_commit_en_1, mul_commit_en_2,
    input wire [31:0] alu_commit_value_1, alu_commit_value_2, mul_commit_value_1, mul_commit_value_2,
    input wire [4:0] alu_commit_save_no_1, alu_commit_save_no_2, mul_commit_save_no_1, mul_commit_save_no_2, 
    input wire [4:0] alu_commit_rob_no_1, alu_commit_rob_no_2, mul_commit_rob_no_1, mul_commit_rob_no_2, 
    output wire [31:0] alu_x_1, alu_y_1, alu_x_2, alu_y_2,
    output wire [3:0] cAlu_ctrl_1, cAlu_ctrl_2,
    output wire alu_ready_en_1, alu_ready_en_2,
    output wire [4:0] alu_save_no_1, alu_save_no_2, alu_rob_no_1, alu_rob_no_2,
    output wire [31:0] complex_x_1, complex_y_1, complex_x_2, complex_y_2,
    output wire [3:0] cComplex_ctrl_1, cComplex_ctrl_2,
    output wire complex_ready_en_1, complex_ready_en_2,
    output wire [4:0] complex_save_no_1, complex_save_no_2, complex_rob_no_1, complex_rob_no_2,
    output wire [31:0] address_x_1, address_y_1, address_x_2, address_y_2,
    output wire [3:0] address_ctrl_1, address_ctrl_2,
    output wire address_ready_en_1, address_ready_en_2,
    output wire [4:0] address_save_no_1, address_save_no_2, address_rob_no_1, address_rob_no_2,
    output wire [31:0] mem_address_1, mem_content_1, mem_address_2, mem_content_2,
    output wire [2:0] mem_rd_ctrl_1, mem_rd_ctrl_2,
    output wire [1:0] mem_wr_ctrl_1, mem_wr_ctrl_2,
    output wire mem_read_en_1, mem_write_en_1, mem_read_en_2, mem_write_en_2,
    output wire [4:0] mem_save_no_1, mem_save_no_2, mem_rob_no_1, mem_rob_no_2
);

    //实例化普通运算保留站
    AluSaveStation alu_save(.clk(clk),
                         .save_en_1(save_en_1), .save_en_2(save_en_2),
                         .pc_1(pc_1), .pc_2(pc_2),
                         .work_mode_1(work_mode_1), .work_mode_2(work_mode_2),
                         .alu_ctrl_1(alu_ctrl_1_sch), .alu_ctrl_2(alu_ctrl_2_sch),
                         .imm_1(imm_1), .imm_2(imm_2),  
                         .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready), 
                         .rs_1_relation(rs_1_relation), .rt_1_relation(rt_1_relation), .rd_1_relation(rd_1_relation), .rs_2_relation(rs_2_relation), .rt_2_relation(rt_2_relation), .rd_2_relation(rd_2_relation),  
                         .rd_1_flag(rd_1_flag), .rd_2_flag(rd_2_flag),
                         .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
                         .alu_commit_en_1(alu_commit_en_1), .alu_commit_en_2(alu_commit_en_2), .mul_commit_en_1(mul_commit_en_1), .mul_commit_en_2(mul_commit_en_2),
                         .alu_commit_value_1(alu_commit_value_1), .alu_commit_value_2(alu_commit_value_2), .mul_commit_value_1(mul_commit_value_1), .mul_commit_value_2(mul_commit_value_2),
                         .alu_commit_save_no_1(alu_commit_save_no_1), .alu_commit_save_no_2(alu_commit_save_no_2), .mul_commit_save_no_1(mul_commit_save_no_1), .mul_commit_save_no_2(mul_commit_save_no_2),
                         .alu_commit_rob_no_1(alu_commit_rob_no_1), .alu_commit_rob_no_2(alu_commit_rob_no_2), .mul_commit_rob_no_1(mul_commit_rob_no_1), .mul_commit_rob_no_2(mul_commit_rob_no_2),
                         .alu_x_1(alu_x_1), .alu_y_1(alu_y_1), .alu_x_2(alu_x_2), .alu_y_2(alu_y_2),
                         .cAlu_ctrl_1(cAlu_ctrl_1), .cAlu_ctrl_2(cAlu_ctrl_2),
                         .alu_ready_en_1(alu_ready_en_1), .alu_ready_en_2(alu_ready_en_2),
                         .alu_save_no_1(alu_save_no_1), .alu_save_no_2(alu_save_no_2), .alu_rob_no_1(alu_rob_no_1), .alu_rob_no_2(alu_rob_no_2));

    //实例化乘除运算保留站
    ComplexSaveStation complex_save(.clk(clk),
                             .save_en_1(save_en_1), .save_en_2(save_en_2),
                             .pc_1(pc_1), .pc_2(pc_2),
                             .work_mode_1(work_mode_1), .work_mode_2(work_mode_2),
                             .complex_ctrl_1(complex_ctrl_1_sch), .complex_ctrl_2(complex_ctrl_2_sch),
                             .imm_1(imm_1), .imm_2(imm_2),
                             .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready), 
                             .rs_1_relation(rs_1_relation), .rt_1_relation(rt_1_relation), .rd_1_relation(rd_1_relation), .rs_2_relation(rs_2_relation), .rt_2_relation(rt_2_relation), .rd_2_relation(rd_2_relation),
                             .rd_1_flag(rd_1_flag), .rd_2_flag(rd_2_flag),
                             .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
                             .alu_commit_en_1(alu_commit_en_1), .alu_commit_en_2(alu_commit_en_2), .mul_commit_en_1(mul_commit_en_1), .mul_commit_en_2(mul_commit_en_2),
                             .alu_commit_value_1(alu_commit_value_1), .alu_commit_value_2(alu_commit_value_2), .mul_commit_value_1(mul_commit_value_1), .mul_commit_value_2(mul_commit_value_2),
                             .alu_commit_save_no_1(alu_commit_save_no_1), .alu_commit_save_no_2(alu_commit_save_no_2), .mul_commit_save_no_1(mul_commit_save_no_1), .mul_commit_save_no_2(mul_commit_save_no_2),
                             .alu_commit_rob_no_1(alu_commit_rob_no_1), .alu_commit_rob_no_2(alu_commit_rob_no_2), .mul_commit_rob_no_1(mul_commit_rob_no_1), .mul_commit_rob_no_2(mul_commit_rob_no_2),
                             .complex_x_1(complex_x_1), .complex_y_1(complex_y_1), .complex_x_2(complex_x_2), .complex_y_2(complex_y_2),
                             .cComplex_ctrl_1(cComplex_ctrl_1), .cComplex_ctrl_2(cComplex_ctrl_2),
                             .complex_ready_en_1(complex_ready_en_1), .complex_ready_en_2(complex_ready_en_2),
                             .complex_save_no_1(complex_save_no_1), .complex_save_no_2(complex_save_no_2), .complex_rob_no_1(complex_rob_no_1), .complex_rob_no_2(complex_rob_no_2));

    //访存运算保留站
    MemSaveStation mem_save(.clk(clk),
                             .save_en_1(save_en_1), .save_en_2(save_en_2),
                             .pc_1(pc_1), .pc_2(pc_2),
                             .work_mode_1(work_mode_1), .work_mode_2(work_mode_2),
                             .imm_1(imm_1), .imm_2(imm_2),
                             .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready), 
                             .rs_1_relation(rs_1_relation), .rt_1_relation(rt_1_relation), .rd_1_relation(rd_1_relation), .rs_2_relation(rs_2_relation), .rt_2_relation(rt_2_relation), .rd_2_relation(rd_2_relation),
                             .rd_1_flag(rd_1_flag), .rd_2_flag(rd_2_flag),
                             .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
                             .alu_commit_en_1(alu_commit_en_1), .alu_commit_en_2(alu_commit_en_2), .mul_commit_en_1(mul_commit_en_1), .mul_commit_en_2(mul_commit_en_2),
                             .alu_commit_value_1(alu_commit_value_1), .alu_commit_value_2(alu_commit_value_2), .mul_commit_value_1(mul_commit_value_1), .mul_commit_value_2(mul_commit_value_2),
                             .alu_commit_save_no_1(alu_commit_save_no_1), .alu_commit_save_no_2(alu_commit_save_no_2), .mul_commit_save_no_1(mul_commit_save_no_1), .mul_commit_save_no_2(mul_commit_save_no_2),
                             .alu_commit_rob_no_1(alu_commit_rob_no_1), .alu_commit_rob_no_2(alu_commit_rob_no_2), .mul_commit_rob_no_1(mul_commit_rob_no_1), .mul_commit_rob_no_2(mul_commit_rob_no_2),
                             .address_x_1(address_x_1), .address_y_1(address_y_1), .address_x_2(address_x_2), .address_y_2(address_y_2),
                             .address_ctrl_1(address_ctrl_1), .address_ctrl_2(address_ctrl_2),
                             .address_ready_en_1(address_ready_en_1), .address_ready_en_2(address_ready_en_2),
                             .address_save_no_1(address_save_no_1), .address_save_no_2(address_save_no_2), .address_rob_no_1(), .address_rob_no_2(address_rob_no_2),
                             .mem_address_1(mem_address_1), .mem_content_1(mem_content_1), .mem_address_2(mem_address_2), .mem_content_2(mem_content_2),
                             .mem_rd_ctrl_1(mem_rd_ctrl_1), .mem_rd_ctrl_2(mem_rd_ctrl_2),
                             .mem_wr_ctrl_1(mem_wr_ctrl_1), .mem_wr_ctrl_2(mem_wr_ctrl_2),
                             .mem_read_en_1(mem_read_en_1), .mem_write_en_1(mem_write_en_1), .mem_read_en_2(mem_read_en_2), .mem_write_en_2(mem_write_en_2),
                             .mem_save_no_1(mem_save_no_1), .mem_save_no_2(mem_save_no_2), .mem_rob_no_1(mem_rob_no_1), .mem_rob_no_2(mem_rob_no_2));
endmodule