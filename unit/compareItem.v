module compareItem (
    input wire [4:0] rob_1_in,
    input wire rob_flag_1_in,
    input wire [4:0] save_no_1_in,
    input wire ready_1_in,
    input wire [4:0] rob_2_in,
    input wire rob_flag_2_in,
    input wire [4:0] save_no_2_in,
    input wire ready_2_in,
    output reg [4:0] rob_out,
    output reg rob_flag_out,
    output reg [4:0] save_no_out,
    output reg ready_out
);

    always @(*) begin
        if (ready_1_in && ready_2_in) begin
            if (rob_flag_1_in ^ rob_flag_2_in) begin
                //flag不同的时候，rob越大，这里的值越旧
                if (rob_1_in > rob_2_in) begin
                    rob_out = rob_1_in;
                    save_no_out = save_no_1_in;
                    rob_flag_out = rob_flag_1_in;
                end
                else begin
                    rob_out = rob_2_in;
                    save_no_out = save_no_2_in;
                    rob_flag_out = rob_flag_2_in;
                end
            end
            else begin
                //flag相同的时候，rob越小，这里的值越旧
                if (rob_1_in < rob_2_in) begin
                    rob_out = rob_1_in;
                    save_no_out = save_no_1_in;
                    rob_flag_out = rob_flag_1_in;
                end
                else begin
                    rob_out = rob_2_in;
                    save_no_out = save_no_2_in;
                    rob_flag_out = rob_flag_2_in;
                end
            end
            ready_out = 1;
        end
        else if (ready_1_in) begin
            rob_out = rob_1_in;
            save_no_out = save_no_1_in;
            ready_out = 1;
            rob_flag_out = rob_flag_1_in;
        end
        else if (ready_2_in) begin
            rob_out = rob_2_in;
            save_no_out = save_no_2_in;
            ready_out = 1;
            rob_flag_out = rob_flag_2_in;
        end
        else begin
            ready_out = 0;
        end
    end
    
endmodule