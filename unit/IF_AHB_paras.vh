`define ITCM_BASE_ADDR  32'h0000_0000_0000_0000
`define ITCM_MAX_ADDR  32'h0000_0000_0000_ffff

`define DTCM_BASE_ADDR 32'h0000_0000_2000_0000
`define DTCM_MAX_ADDR 32'h0000_0000_2000_ffff

`define TRANS_IDLE 2'b00
`define TRANS_NONSEQ 2'b10

`define HSIZE  3'b011
`define HBURST 3'b000
`define HPROT 3'b010

`define RESP_OKAY 2'b00

`define PC_add_insr 1
`define ADDRESS_rst         32'h0000_0000_0000_0000

`define ITCM_DW 64
`define ITCM_DP 512
`define ITCM_MW (`ITCM_DW/8)
`define ITCM_AW 32

`define INSTR_NOP 32'h0000_0000_0000_0013 /* addi x0,x0,0 */