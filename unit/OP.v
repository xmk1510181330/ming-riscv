module OP (
    input wire clk,
    input wire [4:0] rs_1, rt_1, rd_1, rs_2, rt_2, rd_2,
    input wire rob_write_en_1, rob_write_en_2,
    input wire [1:0] rob_write_sel_1, rob_write_sel_2,
    input wire rob_save_direct_ctrl_1, rob_save_direct_ctrl_2,
    input wire [31:0] imm_1_in, imm_2_in,
    input wire save_en_1_in, save_en_2_in,
    input wire [3:0] alu_ctrl_1_in, alu_ctrl_2_in,
    input wire [3:0] complex_ctrl_1_in, complex_ctrl_2_in,
    input wire [2:0] dm_rd_ctrl_1_in, dm_rd_ctrl_2_in,
    input wire [1:0] dm_wr_ctrl_1_in, dm_wr_ctrl_2_in,
    input wire [2:0] work_mode_1_in, work_mode_2_in,
    input wire alu_a_sel_1, alu_b_sel_1, alu_a_sel_2, alu_b_sel_2,
    input wire alu_commit_en, mul_commit_en, div_commit_en, mem_commit_en,
    input wire [4:0] alu_commit_rob_no, mul_commit_rob_no, div_commit_rob_no, mem_commit_rob_no,
    input wire [31:0] alu_commit_value, mul_commit_value, div_commit_value, mem_commit_value,
    input wire [11:0] pc_1, pc_2,
    output wire opt_x_1_ready, opt_y_1_ready, opt_x_2_ready, opt_y_2_ready,
    output wire [31:0] opt_x_1_value, opt_y_1_value, opt_x_2_value, opt_y_2_value,
    output wire [4:0] opt_x_1_rob, opt_y_1_rob, opt_x_2_rob, opt_y_2_rob,        
    output wire rs_1_ready, rt_1_ready, rs_2_ready, rt_2_ready,
    output wire [31:0] rs_1_value, rt_1_value, rs_2_value, rt_2_value,
    output wire rd_1_rob_flag, rd_2_rob_flag,
    output wire [4:0] rs_1_rob, rt_1_rob, rs_2_rob, rt_2_rob, rd_1_rob, rd_2_rob,
    output reg [31:0] imm_1_out, imm_2_out,
    output reg save_en_1_out, save_en_2_out,
    output reg [3:0] alu_ctrl_1_out, alu_ctrl_2_out,
    output reg [3:0] complex_ctrl_1_out, complex_ctrl_2_out,
    output reg [2:0] dm_rd_ctrl_1_out, dm_rd_ctrl_2_out,
    output reg [1:0] dm_wr_ctrl_1_out, dm_wr_ctrl_2_out,
    output reg [2:0] work_mode_1_out, work_mode_2_out
);
    reg [31:0] imm_1_stage1, imm_2_stage1;
    reg save_en_1_stage1, save_en_2_stage1;
    reg [3:0] alu_ctrl_1_stage1, alu_ctrl_2_stage1;
    reg [3:0] complex_ctrl_1_stage1, complex_ctrl_2_stage1;
    reg [2:0] dm_rd_ctrl_1_stage1, dm_rd_ctrl_2_stage1;
    reg [1:0] dm_wr_ctrl_1_stage1, dm_wr_ctrl_2_stage1;
    reg [2:0] work_mode_1_stage1, work_mode_2_stage1;

    reg [31:0] imm_1_stage2, imm_2_stage2;
    reg save_en_1_stage2, save_en_2_stage2;
    reg [3:0] alu_ctrl_1_stage2, alu_ctrl_2_stage2;
    reg [3:0] complex_ctrl_1_stage2, complex_ctrl_2_stage2;
    reg [2:0] dm_rd_ctrl_1_stage2, dm_rd_ctrl_2_stage2;
    reg [1:0] dm_wr_ctrl_1_stage2, dm_wr_ctrl_2_stage2;
    reg [2:0] work_mode_1_stage2, work_mode_2_stage2;


    //实例化寄存器
    registers r1(.clk(clk), .rst(rst), .rs_1(rs_1), .rs_2(rs_2), .rt_1(rt_1), .rt_2(rt_2),
                .rd_1(rd_1), .rd_2(rd_2),
                .imm_1(imm_1_in), .imm_2(imm_2_in),
                .rob_save_direct_ctrl_1(rob_save_direct_ctrl_1), .rob_save_direct_ctrl_2(rob_save_direct_ctrl_2),
                .alu_commit_en(alu_commit_en), .mul_commit_en(mul_commit_en), .div_commit_en(div_commit_en), .mem_commit_en(mem_commit_en),
                .alu_commit_rob_no(alu_commit_rob_no), .mul_commit_rob_no(mul_commit_rob_no), .div_commit_rob_no(div_commit_rob_no), .mem_commit_rob_no(mem_commit_rob_no),
                .alu_commit_value(alu_commit_value), .mul_commit_value(mul_commit_value), .div_commit_value(div_commit_value), .mem_commit_value(mem_commit_value),
                .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready),
                .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
                .rs_rob_no_1(rs_1_rob), .rt_rob_no_1(rt_1_rob), .rs_rob_no_2(rs_2_rob), .rt_rob_no_2(rt_2_rob),
                .rd_rob_flag_1(rd_1_rob_flag), .rd_rob_flag_2(rd_2_rob_flag),
                .rd_rob_no_1(rd_1_rob), .rd_rob_no_2(rd_2_rob));

    //将一些当前阶段用不到的信号暂存一下
    always @(posedge clk) begin
        imm_1_stage1 <= imm_1_in;
        imm_2_stage1 <= imm_2_in;
        save_en_1_stage1 <= save_en_1_in;
        save_en_2_stage1 <= save_en_2_in;
        alu_ctrl_1_stage1 <= alu_ctrl_1_in;
        alu_ctrl_2_stage1 <= alu_ctrl_2_in;
        complex_ctrl_1_stage1 <= complex_ctrl_1_in;
        complex_ctrl_2_stage1 <= complex_ctrl_2_in;
        dm_rd_ctrl_1_stage1 <= dm_rd_ctrl_1_in;
        dm_rd_ctrl_2_stage1 <= dm_rd_ctrl_2_in;
        dm_wr_ctrl_1_stage1 <= dm_wr_ctrl_1_in;
        dm_wr_ctrl_2_stage1 <= dm_wr_ctrl_2_in;
        work_mode_1_stage1 <= work_mode_1_in;
        work_mode_2_stage1 <= work_mode_2_in;
    end

    always @(posedge clk) begin
        imm_1_stage2 <= imm_1_stage1;
        imm_2_stage2 <= imm_2_stage1;
        save_en_1_stage2 <= save_en_1_stage1;
        save_en_2_stage2 <= save_en_2_stage1;
        alu_ctrl_1_stage2 <= alu_ctrl_1_stage1;
        alu_ctrl_2_stage2 <= alu_ctrl_2_stage1;
        complex_ctrl_1_stage2 <= complex_ctrl_1_stage1;
        complex_ctrl_2_stage2 <= complex_ctrl_2_stage1;
        dm_rd_ctrl_1_stage2 <= dm_rd_ctrl_1_stage1;
        dm_rd_ctrl_2_stage2 <= dm_rd_ctrl_2_stage1;
        dm_wr_ctrl_1_stage2 <= dm_wr_ctrl_1_stage1;
        dm_wr_ctrl_2_stage2 <= dm_wr_ctrl_2_stage1;
        work_mode_1_stage2 <= work_mode_1_stage1;
        work_mode_2_stage2 <= work_mode_2_stage1;
    end

    always @(posedge clk) begin
        imm_1_out <= imm_1_stage2;
        imm_2_out <= imm_2_stage2;
        save_en_1_out <= save_en_1_stage2;
        save_en_2_out <= save_en_2_stage2;
        alu_ctrl_1_out <= alu_ctrl_1_stage2;
        alu_ctrl_2_out <= alu_ctrl_2_stage2;
        complex_ctrl_1_out <= complex_ctrl_1_stage2;
        complex_ctrl_2_out <= complex_ctrl_2_stage2;
        dm_rd_ctrl_1_out <= dm_rd_ctrl_1_stage2;
        dm_rd_ctrl_2_out <= dm_rd_ctrl_2_stage2;
        dm_wr_ctrl_1_out <= dm_wr_ctrl_1_stage2;
        dm_wr_ctrl_2_out <= dm_wr_ctrl_2_stage2;
        work_mode_1_out <= work_mode_1_stage2;
        work_mode_2_out <= work_mode_2_stage2;
    end

endmodule