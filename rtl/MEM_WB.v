module MEM_WB (
    input wire clk,
    input wire rst,
    input wire reg_write_en_in,
    input wire [1:0] reg_wr_sel_in,
    input wire [31:0] alu_result_in,
    input wire [31:0] mem_result_in,
    input wire [31:0] pc_in,
    input wire [31:0] inst_in,

    output reg reg_write_en_out,          //写寄存器的使能信号
    output reg [31:0] reg_write_data_out, //写寄存器的内容
    output reg [4:0] reg_write_no_out     //要写入寄存器的编号
);
    
    always @(posedge clk) begin
        reg_write_en_out <= reg_write_en_in;
        reg_write_no_out <= inst_in[11:7];
        //根据选择信号确定要写入寄存器的值
        case (reg_wr_sel_in)
            2'b01: reg_write_data_out <= pc_in + 4;
            2'b10: reg_write_data_out <= alu_result_in;
            2'b11: reg_write_data_out <= mem_result_in;
            2'b00: reg_write_data_out <= 0;
            default: reg_write_data_out <= 0;
        endcase
    end
endmodule