/**
 ** 执行-写回中继器
**/

module EXE_MEM (
    input wire clk,
    input wire rst,
    input wire [31:0] alu_r_in,
    input wire [2:0] dm_rd_ctrl_in,
    input wire [1:0] dm_wr_ctrl_in,
    input wire reg_write_en_in,
    input wire [1:0] reg_write_sel_in,
    input wire [31:0] now_inst_in,
    input wire [31:0] pc_in,
    input wire [31:0] reg_r2_in,

    output reg [31:0] alu_r_out,
    output reg [2:0] dm_rd_ctrl_out,
    output reg [1:0] dm_wr_ctrl_out,
    output reg reg_write_en_out,
    output reg [1:0] reg_write_sel_out,
    output reg [31:0] now_inst_out,
    output reg [31:0] pc_out,
    output reg [31:0] reg_r2_out
);

    always @(posedge clk) begin
        dm_rd_ctrl_out <= dm_rd_ctrl_in;
        dm_wr_ctrl_out <= dm_wr_ctrl_in;
        now_inst_out <= now_inst_in;
        reg_write_en_out <= reg_write_en_in;
        reg_write_sel_out <= reg_write_sel_in;
        pc_out <= pc_in;
        alu_r_out <= alu_r_in;
    end
endmodule