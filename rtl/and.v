/**
 ** 简单的与门
**/

module andDoor (
    input a,
    input b,
    output c
);

    assign c = a & b;
endmodule