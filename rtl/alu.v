/**
 ** 运算器模块
**/
module alu (
    input wire clk,
    input wire rst,
    input wire [31:0] srcA,      //操作数A
    input wire [31:0] srcB,      //操作数B
    input wire [3:0] func,       //功能函数，选择执行具体的哪一项功能
    input wire [31:0] now_inst,  //当前的指令
    output reg [31:0] result,    //运算器的执行效果
    output reg [31:0] alu_inst_out
);
    //定义有符号和无符号的操作数，方便后面运算
    wire signed [31:0] signedSrcA;
    wire signed [31:0] signedSrcB;
    wire unsigned [31:0] unsignedSrcA;
    wire unsigned [31:0] unsignedSrcB;

    assign signedSrcA = srcA;
    assign unsignedSrcA = srcA;
    assign signedSrcB = srcB;
    assign unsignedSrcB = srcB;

    //原来这里加了上升沿触发，但是好像也不太对，时钟对不上
    always @(*) begin
        result = 0;
        //根据选择信号执行不同的运算
        case (func)
            4'b0000: result = signedSrcA + signedSrcB;
            4'b1000: result = signedSrcA - signedSrcB;
            4'b0001: result = signedSrcA << signedSrcB[4:0];
            4'b0010: result = signedSrcA < signedSrcB ? 1 : 0;
            4'b0011: result = unsignedSrcA < unsignedSrcB ? 1 : 0;
            4'b0100: result = signedSrcA ^ signedSrcB;
            4'b0101: result = signedSrcA >> signedSrcB[4:0];
            4'b1101: result = signedSrcA >>> signedSrcB[4:0];
            4'b0110: result = signedSrcA | signedSrcB;
            4'b0111: result = signedSrcA & signedSrcB;
            4'b1110: result = signedSrcB;
            default: result = 0;
        endcase
        //将指令输出
        alu_inst_out = now_inst;
    end
endmodule