/**
 ** 取指-译码中继器
**/

module IF_ID (
    input wire clk,
    input wire rst,
    input wire [31:0] now_inst_in,
    input wire [31:0] pc_in,

    output reg [31:0] now_inst_out,
    output reg [31:0] pc_out
);
    
    always @(posedge clk) begin
        now_inst_out <= now_inst_in;
        pc_out <= pc_in;
    end
endmodule