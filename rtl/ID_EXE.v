/**
 ** 译码-执行中继器
**/

module ID_EXE (
    input wire clk,
    input wire rst,
    input wire [3:0] func_in,
    input wire [31:0] srcA_in,
    input wire [31:0] srcB_in,
    input wire reg_write_en_in,
    input wire [1:0] reg_write_sel_in,
    input wire [2:0] dm_rd_ctrl_in,
    input wire [1:0] dm_wr_ctrl_in,
    input wire [31:0] pc_in,
    input wire [31:0] now_inst_in,
    input wire [31:0] reg_r2_in,

    output reg [3:0] func_out,
    output reg [31:0] srcA_out,
    output reg [31:0] srcB_out,
    output reg reg_write_en_out,
    output reg [1:0] reg_write_sel_out,
    output reg [2:0] dm_rd_ctrl_out,
    output reg [1:0] dm_wr_ctrl_out,
    output reg [31:0] pc_out,
    output reg [31:0] now_inst_out,
    output reg [31:0] reg_r2_out
); 

    always @(posedge clk) begin
        func_out <= func_in;
        srcA_out <= srcA_in;
        srcB_out <= srcB_in;
        reg_write_en_out <= reg_write_en_in;
        reg_write_sel_out <= reg_write_sel_in;
        pc_out <= pc_in;
        dm_rd_ctrl_out <= dm_rd_ctrl_in;
        dm_wr_ctrl_out <= dm_wr_ctrl_in;
        now_inst_out <= now_inst_in;
        reg_r2_out <= reg_r2_in;
    end
    
endmodule