/**
 ** 数据旁路传递器
**/

module byPass (
    input  wire [31:0] alu_a_in,  //当前寄存器中的操作数A
    input  wire [31:0] alu_b_in,  //当前寄存器中的操作数B
    input  wire [31:0] pre_exe_result, //前一步指令的运算器计算结果
    input  wire [31:0] pre_mem_result, //前前一条指令的运算器结果
    input  wire [31:0] inst, //当前指令
    input  wire [31:0] ex_inst, //运算流水级的指令内容
    input  wire [31:0] mem_inst, //访存流水级的指令内容
    output reg [31:0] alu_a_out, //运算器的一个输入
    output reg [31:0] alu_b_out, //运算器的另一个输入
    output reg rs_en,
    output reg rt_en
);

    //如果存在rs或者rt等于前一条指令的rd，则需要将运算器的前递数据直接替换掉原寄存器输出
    wire [4:0] rs;
    wire [4:0] rt;

    wire [4:0] ex_rd;
    wire [4:0] mem_rd;

    assign rs = inst[19:15];
    assign rt = inst[24:20];
    assign ex_rd = ex_inst[11:7];
    assign mem_rd = mem_inst[11:7];
    always @(*) begin
        rs_en = 0;
        rt_en = 0;
        alu_a_out = alu_a_in;
        alu_b_out = alu_b_in;
        if (rs==ex_rd) begin
            alu_a_out = pre_exe_result;
            rs_en = 1;
        end
        else if (rt==ex_rd) begin
            alu_b_out = pre_exe_result;
            rt_en = 1;
        end
        else begin
            
        end
        
        if (rs==mem_rd) begin
            alu_a_out = pre_mem_result;
            rs_en = 1;
        end
        else if(rt==mem_rd) begin
            alu_b_out = pre_mem_result;
            rt_en = 1;
        end
        else begin
            
        end
    end    
endmodule