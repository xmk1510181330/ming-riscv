/**
 ** 寄存器模块
**/
module registers (
    input wire clk,
    input wire rst,
    input wire [4:0] now_a1,  //读or写寄存器号
    input wire [4:0] now_a2,
    input wire [4:0] now_a3,
    input wire [31:0] write_in,      //要写入寄存器的值
    input wire write_en,      //写使能信号
    output reg [31:0] now_rs, //读取出来的寄存器的值
    output reg [31:0] now_rt
);
    // 用数组模拟寄存器堆
    reg [31:0] reg_file[0:31];
    integer i;

    initial begin
        //初始化寄存器
        for (i = 0; i < 32; i++) begin
            reg_file[i] = 0;
        end
    end

    //此处如果加上上升沿处罚，将会导致读取内容受阻，原因不知道
    //写入寄存器
    always@(*) begin
        //写使能时，将数据写入
        if(write_en) begin
            if(now_a3==0)
                reg_file[now_a3] <= 0;
            else
                reg_file[now_a3] <= write_in;
        end

        //读取寄存器
        if (now_a1>=0 && now_a1<32) begin
            now_rs <= reg_file[now_a1];
        end

        if (now_a2>=0 && now_a2<32) begin
            now_rt <= reg_file[now_a2];
        end
    end
endmodule