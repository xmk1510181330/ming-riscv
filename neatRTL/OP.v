module OP (
    input wire [4:0] rs_1, rt_1, rd_1, rs_2, rt_2, rd_2,
    input wire rob_write_en_1, rob_write_en_2,
    input wire [1:0] rob_write_sel_1, rob_write_sel_2,
    input wire rob_save_direct_ctrl_1, rob_save_direct_ctrl_2,
    input wire [31:0] imm_1, imm_2,
    input wire alu_commit_en, mul_commit_en, div_commit_en,
    input wire [4:0] alu_commit_rob_no, mul_commit_rob_no, div_commit_rob_no,
    input wire [31:0] alu_commit_value, mul_commit_value, div_commit_value,
    output wire rs_1_ready, rt_1_ready, rs_2_ready, rt_2_ready,
    output wire [31:0] rs_1_value, rt_1_value, rs_2_value, rt_2_value,
    output wire [4:0] rs_1_rob, rt_1_rob, rs_2_rob, rt_2_rob, rd_1_rob, rd_2_rob
);


    //实例化寄存器
    registers r1(.clk(clk), .rst(rst), .rs_1(rs_1), .rs_2(rs_2), .rt_1(rt_1), .rt_2(rt_2),
                .rd_1(rd_1), .rd_2(rd_2),
                .imm_1(imm_1), .imm_2(imm_2),
                .rob_save_direct_ctrl_1(rob_save_direct_ctrl_1), .rob_save_direct_ctrl_2(rob_save_direct_ctrl_2),
                .alu_commit_en(alu_commit_en), .mul_commit_en(mul_commit_en), .div_commit_en(div_commit_en),
                .alu_commit_rob_no(alu_commit_rob_no), .mul_commit_rob_no(mul_commit_rob_no), .div_commit_rob_no(div_commit_rob_no),
                .alu_commit_value(alu_commit_value), .mul_commit_value(mul_commit_value), .div_commit_value(div_commit_value), 
                .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready),
                .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
                .rs_rob_no_1(rs_1_rob), .rt_rob_no_1(rt_1_rob), .rs_rob_no_2(rs_2_rob), .rt_rob_no_2(rt_2_rob),
                .rd_rob_no_1(rd_1_rob), .rd_rob_no_2(rd_2_rob));
    
endmodule