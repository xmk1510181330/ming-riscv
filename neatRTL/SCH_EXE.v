module SCH_EXE (
    input wire clk,
    input wire rst,
    input wire [4:0] result_rob_no_in,             //结果要写回的ROB
    input wire [3:0] common_ctrl_in,         //对于运算单元要输出的结果
    input wire [31:0] alu_X_in,
    input wire [31:0] alu_Y_in,
    input wire alu_ready_en_in,
    input wire [3:0] mul_ctrl_in,
    input wire [31:0] mul_X_in,
    input wire [31:0] mul_Y_in,
    input wire mul_ready_en_in,
    input wire [3:0] div_ctrl_in,
    input wire [31:0] div_X_in,
    input wire [31:0] div_Y_in,
    input wire div_ready_en_in,
    input wire [4:0] alu_rd_rob_no_in, mul_rd_rob_no_in, div_rd_rob_no_in,
    output reg [4:0] result_rob_no_out,             //结果要写回的ROB
    output reg [3:0] common_ctrl_out,         //对于运算单元要输出的结果
    output reg [31:0] alu_X_out,
    output reg [31:0] alu_Y_out,
    output reg alu_ready_en_out,
    output reg [3:0] mul_ctrl_out,
    output reg [31:0] mul_X_out,
    output reg [31:0] mul_Y_out,
    output reg mul_ready_en_out,
    output reg [3:0] div_ctrl_out,
    output reg [31:0] div_X_out,
    output reg [31:0] div_Y_out,
    output reg div_ready_en_out,
    output reg [4:0] alu_rd_rob_no_out, mul_rd_rob_no_out, div_rd_rob_no_out
);

    always @(posedge clk) begin
        result_rob_no_out <= result_rob_no_in;
        common_ctrl_out <= common_ctrl_in;
        alu_X_out <= alu_X_in;
        alu_Y_out <= alu_Y_in;
        alu_ready_en_out <= alu_ready_en_in;
        mul_ctrl_out <= mul_ctrl_in;
        mul_X_out <= mul_X_in;
        mul_Y_out <= mul_Y_in;
        mul_ready_en_out <= mul_ready_en_in;
        div_ctrl_out <= div_ctrl_in;
        div_X_out <= div_X_in;
        div_Y_out <= div_Y_in;
        div_ready_en_out <= div_ready_en_in;
        alu_rd_rob_no_out <= alu_rd_rob_no_in;
        mul_rd_rob_no_out <= mul_rd_rob_no_in;
        div_rd_rob_no_out <= div_rd_rob_no_in;
    end
endmodule