/**
 ** 取指模块
 ** 在这个模块中，需要完成指令读取和分支预测两个功能
**/

module IF (
    input wire clk,
    input wire rst,
    input wire [11:0] pc_1,
    input wire [11:0] pc_2,
    output wire [31:0] inst_1,
    output wire [31:0] inst_2
); 
    //从指令缓存中取到的指令内容
    wire [31:0] mem_inst_1, mem_inst_2;
    //分支预测器的预测结果
    wire [31:0] predict_inst_1, predict_inst_2;
    //分支预测器的预测结果
    wire predict_flag_1, predict_flag_2;
    
    //实例化指令缓存
    instCache icache1(.clk(clk), .pc_1(pc_1), .pc_2(pc_2), .inst_one(mem_inst_1), .inst_two(mem_inst_2));

    //实例化分支预测器
    branchPredictionUnit branchPrediction1(.clk(clk), .rst(rst), .pc_address(pc_1), .pc_address2(pc_2), .branch_taken(), .branch_traget(), .branch_taken2(), .branch_traget2(), .branch_prediction(predict_flag_1), .branch_prediction_traget(predict_inst_1), .branch_prediction2(predict_flag_2), .branch_prediction_traget2(predict_inst_2));

    //设置输出的指令
    assign inst_1 = predict_flag_1==1 ? predict_inst_1 : mem_inst_1;
    assign inst_2 = predict_flag_2==1 ? predict_flag_2 : mem_inst_2;
endmodule