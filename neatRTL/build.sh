echo "开始文件编译🏯"
iverilog -o wave *.v
echo "编译完成! 仿真模拟启动🚀"

echo "生成波形文件"
vvp -n wave -lxt2 
cp tb.vcd tb.lxt

echo "打开波形文件"
open tb.vcd