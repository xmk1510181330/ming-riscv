/**
 ** 半加器的逻辑
**/

module halfaddr (
    input wire a,
    input wire b,
    output wire s,
    output wire carry_out
);
    //根据真值表使用纯逻辑电路实现半加器逻辑
    assign carry_out = a & b;
    assign s = a ^ b;
endmodule