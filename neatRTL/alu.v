/**
 ** 运算器模块，调整一下，第二个时钟在写回

**/
module alu (
    input wire clk,
    input wire rst,
    input wire data_ready,        //要进行运算的数据是否准备就绪的标记
    input wire [31:0] x,         //操作数A
    input wire [31:0] y,         //操作数B
    input wire [3:0] ctrl,       //功能函数，选择执行具体的哪一项功能
    input wire [4:0] save_no_in, //保留站号
    input wire [4:0] rd_rob_in,  //结果影响到的ROB
    output reg done,             //运算器操作的完成信号
    output reg [4:0] save_no_out,
    output reg [4:0] rd_rob_out,
    output reg [31:0] result     //运算器的执行效果
);

    always @(*) begin
        //根据控制信号产生对应的运算结果
        case (ctrl)
            4'b0000: result = x + y;
            4'b1000: result = x - y;
            4'b0001: result = x << y[4:0];
            4'b0010: result = x < y ? 1 : 0;
            4'b0011: result = x < y ? 1 : 0;
            4'b0100: result = x ^ y;
            4'b0101: result = x >> y[4:0];
            4'b1101: result = x >>> y[4:0];
            4'b0110: result = x | y;
            4'b0111: result = x & y;
            4'b1110: result = y;
            default: result = 0;
        endcase
        
        //输出就绪信号
        done = data_ready;
        rd_rob_out = rd_rob_in;
        save_no_out = save_no_in;
    end
endmodule