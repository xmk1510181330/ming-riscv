module divider (
    input wire clk,
    input wire rst,
    input wire data_ready,       //要进行运算的数据是否准备就绪的标记
    input wire [31:0] x,         //操作数A
    input wire [31:0] y,         //操作数B
    input wire [3:0] ctrl,       //功能函数，选择执行具体的哪一项功能
    input wire [4:0] save_no_in, //保留站号
    input wire [4:0] rd_rob_in,  //结果影响到的ROB
    output reg done,             //运算器操作的完成信号
    output reg [4:0] save_no_out,
    output reg [4:0] rd_rob_out,
    output reg [31:0] result     //运算器的执行结果
);
    reg [63:0] temp_a;
    reg [63:0] temp_b;
    integer i;

    //运算逻辑
    always @(*) begin
        temp_a = {32'h00000000,x};
        temp_b = {y,32'h00000000};

        for (i=0; i<32; i++) begin
            temp_a = {temp_a[62:0],1'b0};
            if (temp_a[63:32] >= y) begin
                temp_a = temp_a - temp_b + 1'b1;
            end
            else begin
                temp_a = temp_a;
            end
        end

        //根据控制信号输出结果
        if (ctrl==4'b0100) begin
            result = temp_a[31:0];
        end
        else if (ctrl==4'b0110) begin
            result = temp_a[63:32];
        end
        else begin
            result = 0;
        end

        //输出额外的控制信号
        done = data_ready;
        rd_rob_out = rd_rob_in;
        save_no_out = save_no_in;
    end
endmodule