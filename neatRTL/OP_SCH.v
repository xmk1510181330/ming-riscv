module OP_SCH (
    input wire clk, rst,
    input wire rs_1_ready_in, rt_1_ready_in, rs_2_ready_in, rt_2_ready_in,
    input wire [31:0] rs_1_value_in, rt_1_value_in, rs_2_value_in, rt_2_value_in,
    input wire [4:0] rs_1_rob_in, rt_1_rob_in, rs_2_rob_in, rt_2_rob_in, rd_1_rob_in, rd_2_rob_in,
    input wire [2:0] alu_mode_1_in, alu_mode_2_in,
    input wire [3:0] alu_ctrl_1_in, alu_ctrl_2_in, complex_ctrl_1_in, complex_ctrl_2_in,
    input wire alu_commit_en_in, mul_commit_en_in, div_commit_en_in,
    input wire [4:0] alu_commit_save_no_in, mul_commit_save_no_in, div_commit_save_no_in,
    input wire [4:0] alu_commit_rob_no_in, mul_commit_rob_no_in, div_commit_rob_no_in, 
    input wire [31:0] alu_commit_value_in, mul_commit_value_in, div_commit_value_in, 
    output reg rs_1_ready_out, rt_1_ready_out, rs_2_ready_out, rt_2_ready_out,
    output reg [31:0] rs_1_value_out, rt_1_value_out, rs_2_value_out, rt_2_value_out,
    output reg [4:0] rs_1_rob_out, rt_1_rob_out, rs_2_rob_out, rt_2_rob_out, rd_1_rob_out, rd_2_rob_out,
    output reg [2:0] alu_mode_1_out, alu_mode_2_out, 
    output reg [3:0] alu_ctrl_1_out, alu_ctrl_2_out, complex_ctrl_1_out, complex_ctrl_2_out,
    output reg save_en_1, save_en_2,  //保留站使能信号
    output reg alu_commit_en_out, mul_commit_en_out, div_commit_en_out,
    output reg [4:0] alu_commit_save_no_out, mul_commit_save_no_out, div_commit_save_no_out,
    output reg [4:0] alu_commit_rob_no_out, mul_commit_rob_no_out, div_commit_rob_no_out, 
    output reg [31:0] alu_commit_value_out, mul_commit_value_out, div_commit_value_out
);
    
    always @(posedge clk) begin
        rs_1_ready_out <= rs_1_ready_in;
        rt_1_ready_out <= rt_1_ready_in;
        rs_2_ready_out <= rs_2_ready_in;
        rt_2_ready_out <= rt_2_ready_in;
        rs_1_value_out <= rs_1_value_in;
        rt_1_value_out <= rt_1_value_in;
        rs_2_value_out <= rs_2_value_in;
        rt_2_value_out <= rt_2_value_in;
        rs_1_rob_out <= rs_1_rob_in;
        rt_1_rob_out <= rt_1_rob_in;
        rs_2_rob_out <= rs_2_rob_in;
        rt_2_rob_out <= rt_2_rob_in;
        rd_1_rob_out <= rd_1_rob_in;
        rd_2_rob_out <= rd_2_rob_in;
        alu_ctrl_1_out <= alu_ctrl_1_in;
        alu_ctrl_2_out <= alu_ctrl_2_in;
        complex_ctrl_1_out <= complex_ctrl_1_in;
        complex_ctrl_2_out <= complex_ctrl_2_in;
        alu_mode_1_out <= alu_mode_1_in;
        alu_mode_2_out <= alu_mode_2_in;
        alu_commit_en_out <= alu_commit_en_in;
        mul_commit_en_out <= mul_commit_en_in;
        div_commit_en_out <= div_commit_en_in;
        alu_commit_rob_no_out <= alu_commit_rob_no_in;
        mul_commit_rob_no_out <= mul_commit_rob_no_in;
        div_commit_rob_no_out <= div_commit_rob_no_in;
        alu_commit_value_out <= alu_commit_value_in;
        mul_commit_value_out <= mul_commit_value_in;
        div_commit_value_out <= div_commit_value_in;
        alu_commit_save_no_out <= alu_commit_save_no_in;
        mul_commit_save_no_out <= mul_commit_save_no_in;
        div_commit_save_no_out <= div_commit_save_no_in;


        //如果是运算指令，则需要开启使能信号，保存到保留站
        if (alu_mode_1_in==3'b000 || alu_mode_1_in==3'b001 || alu_mode_1_in==3'b010) begin
            save_en_1 <= 1;
        end
        else begin
            save_en_1 <= 0;
        end

        if (alu_mode_2_in==3'b000 || alu_mode_2_in==3'b001 || alu_mode_2_in==3'b010) begin
            save_en_2 <= 1;
        end
        else begin
            save_en_2 <= 0;
        end
    end
endmodule