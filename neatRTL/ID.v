module ID (
    input wire [31:0] inst_1,
    input wire [31:0] inst_2,
    output wire [4:0] reg_rs_1, reg_rt_1, reg_rd_1,
    output wire rob_write_en_1,
    output wire [1:0] rob_write_sel_1,
    output wire alu_a_sel_1, alu_b_sel_1,
    output wire [2:0] alu_mode_1,
    output wire [3:0] alu_ctrl_1, complex_ctrl_1,
    output wire [2:0] dm_rd_ctrl_1,
    output wire [1:0] dm_wr_ctrl_1,
    output wire rob_save_direct_ctrl_1,
    output wire [31:0] imm_out_1,
    output wire [4:0] reg_rs_2, reg_rt_2, reg_rd_2,
    output wire rob_write_en_2,
    output wire [1:0] rob_write_sel_2,
    output wire alu_a_sel_2, alu_b_sel_2,
    output wire [2:0] alu_mode_2,
    output wire [3:0] alu_ctrl_2, complex_ctrl_2,
    output wire [2:0] dm_rd_ctrl_2,
    output wire [1:0] dm_wr_ctrl_2,
    output wire rob_save_direct_ctrl_2,
    output wire [31:0] imm_out_2
);
    // 立即数控制信号
    wire [2:0] imm_ctrl_1, imm_ctrl_2;
    //从指令中取出寄存器号，如果是没有rs和rt的指令，则将寄存器号设置为0
    assign reg_rs_1 = alu_a_sel_1==1? inst_1[19:15] : 0;
    assign reg_rt_1 = alu_b_sel_1==0? inst_1[24:20] : 0;
    assign reg_rd_1 = inst_1[11:7];

    assign reg_rs_2 = alu_a_sel_2==1? inst_2[19:15] : 0;
    assign reg_rt_2 = alu_b_sel_2==0? inst_2[24:20] : 0;
    assign reg_rd_2 = inst_2[11:7];

    //实例化译码器1
    commonControl idCtrl1(.now_inst(inst_1), .pc(inst_1), .rob_write_en(rob_write_en_1), .rob_write_sel(rob_write_sel_1), .alu_a_sel(alu_a_sel_1), .alu_b_sel(alu_b_sel_1), .alu_mode(alu_mode_1), .alu_ctrl(alu_ctrl_1), .complex_ctrl(complex_ctrl_1), .dm_rd_ctrl(dm_rd_ctrl_1), .dm_wr_ctrl(dm_wr_ctrl_1), .rob_save_direct_ctrl(rob_save_direct_ctrl_1), .imm_ctrl(imm_ctrl_1));
    //实例化译码器2
    commonControl idCtrl2(.now_inst(inst_2), .pc(inst_2), .rob_write_en(rob_write_en_2), .rob_write_sel(rob_write_sel_2), .alu_a_sel(alu_a_sel_2), .alu_b_sel(alu_b_sel_2), .alu_mode(alu_mode_2), .alu_ctrl(alu_ctrl_2), .complex_ctrl(complex_ctrl_2), .dm_rd_ctrl(dm_rd_ctrl_2), .dm_wr_ctrl(dm_wr_ctrl_2), .rob_save_direct_ctrl(rob_save_direct_ctrl_2), .imm_ctrl(imm_ctrl_2));

    //实例化立即数填充单元1
    immPadding imm1(.imm_ctrl(imm_ctrl_1), .now_inst(inst_1), .imm_out(imm_out_1));
    //实例化立即数填充单元2
    immPadding imm2(.imm_ctrl(imm_ctrl_2), .now_inst(inst_2), .imm_out(imm_out_2));
endmodule