
`include "IF_AHB_paras.vh"

module ITCM_top(
    input clk,
    input rst_n,
    
    input i_hsel,
    input i_hwrite,
    input [31:0] i_haddr,
    input [1:0] i_htrans,
    output i_hready,
    output [1:0] i_hresp,
    output [63:0] i_hrdata
    
);
reg sel_en;

reg i_hready_reg;
reg i_hready_reg_nx;
reg [1:0]i_hresp_reg;
reg [1:0]i_hresp_reg_nx;

assign i_hready = i_hready_reg;
assign i_hresp = i_hresp_reg;

always@(*) begin
    sel_en = 1'b0;
    i_hready_reg_nx = 1'b0;
    i_hresp_reg_nx = `RESP_OKAY;
    if(i_hsel && (i_htrans == `TRANS_NONSEQ)) begin
        sel_en = 1'b1;
        i_hready_reg_nx = 1'b1;
        i_hresp_reg_nx = `RESP_OKAY;
    end
end

always@(posedge clk or negedge rst_n) begin
    if(~rst_n) begin
        i_hready_reg <= 1'b0;
        i_hresp_reg <= `RESP_OKAY;
    end
    else begin
        i_hready_reg <= i_hready_reg_nx;
        i_hresp_reg <= i_hresp_reg_nx;
    end
end
itcm_sram # (
    .DP(`ITCM_DP),
    .DW(`ITCM_DW),
    .MW(`ITCM_MW),
    .AW(`ITCM_AW)
)inst_itcm_sram(
    .clk(clk),
    .din('d0),
    .addr(i_haddr),
    .cs(sel_en),
    .we(i_hwrite),
    .wem(8'hff),
    .dout(i_hrdata)    
); 


endmodule
