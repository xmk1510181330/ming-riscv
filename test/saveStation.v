/**
 ** 保留站的实现逻辑
 ** 标记一下，这里应该准备三个就绪队列，分别对应alu，乘法和除法
 ** 每次时钟上升沿，从队列中取出一个就绪块，送入到执行单元，运算得到结果
**/

module saveStation (
    input wire clk,
    input wire save_en_1,                  //保存到保留站的使能信号
    input wire save_en_2,
    input wire [11:0] pc_1, pc_2,
    input wire [2:0] work_mode_1,           //运算模式--普通整数运算 or 整数乘除法 or 浮点
    input wire [2:0] work_mode_2,
    input wire [3:0] alu_ctrl_1,           //运算器的控制信号
    input wire [3:0] alu_ctrl_2,
    input wire [3:0] complex_ctrl_1,       //乘除法的控制信号
    input wire [3:0] complex_ctrl_2,    
    input wire [31:0] imm_1, imm_2,        //立即数的输入  
    input wire rs_1_ready,                 //寄存器是否准备就绪的标记
    input wire rt_1_ready,             
    input wire rs_2_ready,             
    input wire rt_2_ready,             
    input wire [4:0] rs_1_relation,    //和rs寄存器关联的ROB号
    input wire [4:0] rt_1_relation,    //和rt寄存器关联的ROB号
    input wire [4:0] rd_1_relation,    //和rd寄存器关联的ROB号
    input wire [4:0] rs_2_relation,    //和rs寄存器关联的ROB号
    input wire [4:0] rt_2_relation,    //和rt寄存器关联的ROB号
    input wire [4:0] rd_2_relation,    //和rd寄存器关联的ROB号
    input wire [31:0] rs_1_value,      //寄存器的值
    input wire [31:0] rt_1_value,      
    input wire [31:0] rs_2_value,      //寄存器的值
    input wire [31:0] rt_2_value,
    input wire [2:0] dm_rd_ctrl_1, dm_rd_ctrl_2,  //RAM读取控制信号
    input wire [1:0] dm_wr_ctrl_1, dm_wr_ctrl_2,  //RAM写入控制信号
    input wire alu_commit_en, mul_commit_en, div_commit_en, specific_commit_en, mem_commit_en,
    input wire [4:0] alu_commit_save_no, mul_commit_save_no, div_commit_save_no, specific_commit_save_no, mem_commit_save_no,
    input wire [4:0] alu_commit_rob_no, mul_commit_rob_no, div_commit_rob_no, mem_commit_rob_no, 
    input wire [31:0] alu_commit_value, mul_commit_value, div_commit_value, specific_commit_value, mem_commit_value,
    output reg [3:0] common_ctrl,      
    output reg [31:0] alu_X, alu_Y,
    output reg alu_ready_en, div_ready_en, mul_ready_en, specific_ready_en,
    output reg [4:0] alu_rd_rob_no, mul_rd_rob_no, div_rd_rob_no, 
    output reg [4:0] alu_save_no, mul_save_no, div_save_no, specific_save_no,
    output reg [3:0] mul_ctrl,
    output reg [31:0] mul_X, mul_Y,
    output reg [3:0] div_ctrl,
    output reg [31:0] div_X, div_Y,
    output reg [3:0] specific_ctrl,
    output reg [31:0] specific_X, specific_Y,
    output reg read_en_mem, write_en_mem,              //RAM读写使能信号
    output reg [2:0] dm_rd_ctrl_mem,
    output reg [1:0] dm_wr_ctrl_mem,
    output reg [31:0] dm_addr_mem, dm_din_mem,
    output reg [4:0] save_no_mem, 
    output reg [4:0] rd_rob_no_mem
);
    //保留站的一些数组，用来存储在此地等待的指令的信息
    reg [31:0] rs_ok, rt_ok;                                   //寄存器是否准备好的标记位
    reg [31:0] rs_value[0:31], rt_value[0:31], imm[0:31];      //寄存器对应的值
    reg [4:0] rs_relation[0:31], rt_relation[0:31], rd_relation[0:31];  
    reg [11:0] pc[0:31];
    reg [3:0] alu_ctrl[0:31];                                  //运算器的控制信号
    reg [2:0] work_mode[0:31];                                 //运算模式，用来选择运算器
    reg [1:0] dm_write_ctrl[0:31];                             //RAM的控制信号，用来实现数据的存取
    reg [2:0] dm_read_ctrl[0:31];
    reg [31:0] specific_value [0:31];                          //非算数指令运算部分的运算结果
    reg [31:0] specific_ok;                                    //非算数指令运算部分结果就绪位


    //定义空闲队列，方便查找到空闲可用的保留站条目
    reg [4:0] freeQueue[0:31];
    reg [4:0] free_tail;
    reg [4:0] free_head;
    reg [4:0] free_temp_index;

    //定义就绪队列，方便查找到已经就绪的条目，可直接供给运算单元
    reg [4:0] okQueue[0:31];
    reg [4:0] ok_tail;
    reg [4:0] ok_head;
    reg [4:0] ok_temp_index;

    //普通运算器就绪队列及相关指针
    reg [4:0] alu_ok_queue [0:15];
    reg [4:0] alu_ok_tail;
    reg [4:0] alu_ok_head;
    reg [4:0] alu_ok_temp_index;

    //专用运算器就绪队列及相关指针
    reg [4:0] specific_ok_queue [0:15];
    reg [4:0] specific_ok_tail;
    reg [4:0] specific_ok_head;
    reg [4:0] specific_ok_temp_index; 
    //专用运算器的操作数和控制信号
    reg [31:0] specific_ok_x [0:15];
    reg [31:0] specific_ok_y [0:15];
    reg [3:0] specific_ok_ctrl [0:15];

    //整数乘法器就绪队列及相关指针
    reg [4:0] mul_ok_queue [0:15];
    reg [4:0] mul_ok_tail;
    reg [4:0] mul_ok_head;
    reg [4:0] mul_ok_temp_index;

    //整数除法器就绪队列及相关指针
    reg [4:0] div_ok_queue [0:15];
    reg [4:0] div_ok_tail;
    reg [4:0] div_ok_head;
    reg [4:0] div_ok_temp_index;

    //访存就绪队列
    reg [4:0] mem_ok_queue [0:15];
    reg [4:0] mem_ok_tail;
    reg [4:0] mem_ok_head;
    reg [4:0] mem_ok_temp_index;


    //定义Hash表，方便查出与ROB保留站关联的条目
    reg [4:0] map[0:31][0:7];          //32个ROB，每一个ROB对应最多八个关联
    reg [5:0] mapLen[0:31];            //映射表的长度，mapLen[i]表示i号ROB关联保留站的个数
    //临时变量
    reg [5:0] temp_len;
    reg [4:0] temp_no;
    reg [4:0] alu_temp_map_no, mul_temp_map_no, div_temp_map_no, mem_temp_map_no;
    reg reg_temp_ready_1, reg_temp_ready_2, reg_temp_ready_3;

    reg signed [31:0] branch_result;

    //遍历需要的指针
    integer i, j, k;
    

    initial begin
        free_temp_index = 0;
        //初始化空闲队列指针
        free_head = 0;
        free_tail = 31;  //初始情况下，所有的保留站条目都是可用的，即队列是满的
        //初始化就绪队列指针
        ok_head = 0;
        ok_tail = 0;
        alu_ok_tail = 0;
        alu_ok_head = 0;
        mul_ok_tail = 0;
        mul_ok_head = 0;
        div_ok_tail = 0;
        div_ok_head = 0;
        mem_ok_head = 0;
        mem_ok_tail = 0;
        specific_ok_head = 0;
        specific_ok_tail = 0;
        //初始化保留站和队列
        for (i=0; i<32; i++) begin
            rs_ok[i] = 0; 
            rs_ok[i] = 0;
            specific_ok[i] = 0;
            freeQueue[i] = i;
            mapLen[i] = 0;
        end
    end

    //记录写入保留站
    always @(rd_1_relation or rd_2_relation or rs_1_relation or rt_1_relation or rs_2_relation or rt_2_relation or imm_1 or imm_2 or work_mode_1 or work_mode_2) begin
        if (save_en_1==1) begin
            //处理第一条指令
            //从空闲队列取出一个条目供当前记录使用
            free_temp_index = freeQueue[free_head];
            //将数据写入到保留站备用
            rs_value[free_temp_index] = rs_1_value;
            rt_value[free_temp_index] = rt_1_value;
            rd_relation[free_temp_index] = rd_1_relation;
            work_mode[free_temp_index] = work_mode_1;
            pc[free_temp_index] = pc_1;
            imm[free_temp_index] = imm_1;
            if (work_mode_1 == 3'b000 || work_mode_1 == 3'b011 || work_mode_1 == 3'b110) begin   //普通alu运算
                alu_ctrl[free_temp_index] = alu_ctrl_1;
            end
            else if (work_mode_1 == 3'b001 || work_mode_1 == 3'b010) begin  //乘除法
                alu_ctrl[free_temp_index] = complex_ctrl_1;  
            end
            else if (work_mode_1 == 3'b100 || work_mode_1 == 3'b101) begin  //访存指令
                dm_write_ctrl[free_temp_index] = dm_wr_ctrl_1;
                dm_read_ctrl[free_temp_index] = dm_rd_ctrl_1;
                alu_ctrl[free_temp_index] = 4'b0000;
            end
            else begin
                alu_ctrl[free_temp_index] = alu_ctrl_1;
            end

            if (rs_1_ready==1) begin 
                //操作数rs，已经准备好
                rs_ok[free_temp_index] = 1;  
                //标记其未准备好
                specific_ok[free_temp_index] = 0;
                //如果是访存指令，则可以将其发射到特殊运算队列
                if (work_mode_1==3'b100 || work_mode_1==3'b101) begin
                    specific_ok_queue[specific_ok_tail] = free_temp_index;
                    specific_ok_x[specific_ok_tail] = rs_value[free_temp_index];
                    specific_ok_y[specific_ok_tail] = imm[free_temp_index];
                    specific_ok_ctrl[specific_ok_tail] = alu_ctrl[free_temp_index];
                    specific_ok_tail = (specific_ok_tail+1)%16;
                end 
                else if (work_mode_1==3'b011) begin
                    //将立即数运算加入到运算队列
                    rt_value[free_temp_index] = imm[free_temp_index];
                    rt_ok[free_temp_index] = 1;
                    alu_ok_queue[alu_ok_tail] = free_temp_index;
                    alu_ok_tail = (alu_ok_tail+1)%16;
                end
                else begin
                    
                end
            end
            else begin
                rs_ok[free_temp_index] = 0;
                rs_relation[free_temp_index] = rs_1_relation;
                //维护ROB关联映射表
                map[rs_1_relation][mapLen[rs_1_relation]] = free_temp_index;
                mapLen[rs_1_relation] = (mapLen[rs_1_relation]+1)%8;
            end

            if (rt_1_ready==1) begin
                //操作数rt，已经准备好
                rt_ok[free_temp_index] = 1;
            end
            else begin
                rt_ok[free_temp_index] = 0;
                rt_relation[free_temp_index] = rt_1_relation;
                //维护ROB关联映射表
                map[rt_1_relation][mapLen[rt_1_relation]] = free_temp_index;
                mapLen[rt_1_relation] = (mapLen[rt_1_relation]+1)%8;
            end            
          
            if (rs_1_ready==1 && rt_1_ready==1) begin
                //根据所运行模式，加入到不同的队列中
                if (work_mode_1 == 3'b000 || work_mode_1 == 3'b110) begin  //普通alu运算
                    alu_ok_queue[alu_ok_tail] = free_temp_index;
                    alu_ok_tail = (alu_ok_tail+1)%16;
                end
                else if (work_mode_1 == 3'b001) begin  //整数乘法
                    mul_ok_queue[mul_ok_tail] = free_temp_index;  
                    mul_ok_tail = (mul_ok_tail+1)%16;
                end
                else if (work_mode_1 == 3'b010) begin  //整数除法
                    div_ok_queue[div_ok_tail] = free_temp_index;
                    div_ok_tail = (div_ok_tail+1)%16;
                end
                else begin
                    //TODO 后面可能会支持浮点运算之类的
                end
            end

            //指针后移，使用下一块空闲条目
            free_head=(free_head+1)%32;
        end

        if (save_en_2==1) begin
            //处理第二条指令
            //从空闲队列取出一个条目供当前记录使用
            free_temp_index = freeQueue[free_head];
            //将数据写入到保留站备用
            rs_value[free_temp_index] = rs_2_value;
            rt_value[free_temp_index] = rt_2_value;
            rd_relation[free_temp_index] = rd_2_relation;
            work_mode[free_temp_index] = work_mode_2;
            imm[free_temp_index] = imm_2;
            pc[free_temp_index] = pc_2;
            if (work_mode_2 == 3'b000 || work_mode_2 == 3'b011 || work_mode_2 == 3'b110) begin   //普通alu运算
                alu_ctrl[free_temp_index] = alu_ctrl_2;
            end
            else if (work_mode_2 == 3'b001 || work_mode_2 == 3'b010) begin  //乘除法
                alu_ctrl[free_temp_index] = complex_ctrl_2;  
            end
            else if (work_mode_2 == 3'b100 || work_mode_2 == 3'b101) begin
                dm_write_ctrl[free_temp_index] = dm_wr_ctrl_2;
                dm_read_ctrl[free_temp_index] = dm_rd_ctrl_2;
            end
            else begin
                alu_ctrl[free_temp_index] = alu_ctrl_2;
            end

            if (rs_2_ready==1) begin
                //$display("rs的值: ", rs_2_value, " 工作模式: ", work_mode_2);
                rs_ok[free_temp_index] = 1;
                //标记其未准备好
                specific_ok[free_temp_index] = 0;
                //如果是访存指令，则可以将其发射到特殊运算队列
                if (work_mode_2==3'b100 || work_mode_2==3'b101) begin
                    specific_ok_queue[specific_ok_tail] = free_temp_index;
                    specific_ok_x[specific_ok_tail] = rs_value[free_temp_index];
                    specific_ok_y[specific_ok_tail] = imm[free_temp_index];
                    specific_ok_ctrl[specific_ok_tail] = 4'b0000;
                    specific_ok_tail = (specific_ok_tail+1)%16;
                end 
                else if (work_mode_2==3'b011) begin
                    //$display("立即数加入就绪队列: ");
                    //将立即数运算加入到运算队列
                    rt_value[free_temp_index] = imm[free_temp_index];
                    rt_ok[free_temp_index] = 1;
                    alu_ok_queue[alu_ok_tail] = free_temp_index;
                    alu_ok_tail = (alu_ok_tail+1)%16;
                end
                else begin
                    
                end
            end
            else begin
                rs_ok[free_temp_index] = 0;
                rs_relation[free_temp_index] = rs_2_relation;
                //维护ROB关联映射表
                map[rs_2_relation][mapLen[rs_2_relation]] = free_temp_index;
                mapLen[rs_2_relation] = (mapLen[rs_2_relation]+1)%8;
                //$display(rs_2_relation, " -- ", mapLen[rs_2_relation]);
            end

            if (rt_2_ready==1) begin
                rt_ok[free_temp_index] = 1;
            end
            else begin
                rt_ok[free_temp_index] = 0;
                rt_relation[free_temp_index] = rt_2_relation;
                //维护ROB关联映射表
                map[rt_2_relation][mapLen[rt_2_relation]] = free_temp_index;
                mapLen[rt_2_relation] = (mapLen[rt_2_relation]+1)%8;
            end          
            
            //如果两个操作都准备好了，就放入就绪队列
            if (rs_2_ready==1 && rt_2_ready==1) begin
                //$display("保留站号: ", free_temp_index);
                //根据所运行模式，加入到不同的队列中
                if (work_mode_2 == 3'b000 || work_mode_2 == 3'b110) begin  //普通alu运算
                    alu_ok_queue[alu_ok_tail] = free_temp_index;
                    alu_ok_tail = (alu_ok_tail+1)%16;
                end
                else if (work_mode_2 == 3'b001) begin  //整数乘法
                    mul_ok_queue[mul_ok_tail] = free_temp_index;  
                    mul_ok_tail = (mul_ok_tail+1)%16;
                end
                else if (work_mode_2 == 3'b010) begin  //整数除法
                    div_ok_queue[div_ok_tail] = free_temp_index;
                    div_ok_tail = (div_ok_tail+1)%16;
                end
                else begin
                    //TODO 后面可能会支持浮点运算之类的
                end
            end

            //指针后移，使用下一块空闲条目
            free_head=(free_head+1)%32;
        end
    end

    //发射逻辑，每个时钟上升沿输出一个就绪指令
    always @(posedge clk) begin
        //乘法操作
        if (mul_ok_head != mul_ok_tail) begin
            mul_ok_temp_index = mul_ok_queue[mul_ok_head];
            mul_ok_head = (mul_ok_head+1)%16;
            mul_ctrl = alu_ctrl[mul_ok_temp_index];
            mul_X = rs_value[mul_ok_temp_index];
            mul_Y = rt_value[mul_ok_temp_index];
            mul_save_no = mul_ok_temp_index;
            mul_rd_rob_no = rd_relation[mul_ok_temp_index];
            mul_ready_en = 1;
        end
        else begin
            mul_ready_en = 0;
        end
    end

    always @(posedge clk) begin
        //除法操作
        if (div_ok_head != div_ok_tail) begin
            div_ok_temp_index = div_ok_queue[div_ok_head];
            div_ok_head = (div_ok_head+1)%16;
            div_ctrl = alu_ctrl[div_ok_temp_index];
            div_X = rs_value[div_ok_temp_index];
            div_Y = rt_value[div_ok_temp_index];
            div_save_no = div_ok_temp_index;
            div_rd_rob_no = rd_relation[div_ok_temp_index];
            div_ready_en = 1;
        end
        else begin
            div_ready_en = 0;
        end
    end

    always @(posedge clk) begin
        //普通ALU操作
        //$display("就绪队列头指针: ", alu_ok_head, " -尾指针: ", alu_ok_tail);
        if (alu_ok_head != alu_ok_tail) begin  //判断队列不为空时
            alu_ok_temp_index = alu_ok_queue[alu_ok_head];
            alu_ok_head = (alu_ok_head+1)%16;
            common_ctrl = alu_ctrl[alu_ok_temp_index];
            alu_X = rs_value[alu_ok_temp_index];
            alu_Y = rt_value[alu_ok_temp_index];
            alu_save_no = alu_ok_temp_index;
            alu_rd_rob_no = rd_relation[alu_ok_temp_index];
            alu_ready_en = 1;
        end
        else begin
            alu_ready_en = 0; 
        end
    end

    always @(posedge clk) begin
        //访存操作
        if (mem_ok_head != mem_ok_tail) begin
            mem_ok_temp_index = mem_ok_queue[mem_ok_head];
            mem_ok_head = (mem_ok_head+1)%16;
            dm_rd_ctrl_mem = dm_read_ctrl[mem_ok_temp_index];
            dm_wr_ctrl_mem = dm_write_ctrl[mem_ok_temp_index];
            dm_addr_mem = specific_value[mem_ok_temp_index];
            dm_din_mem = rt_value[mem_ok_temp_index];
            save_no_mem = mem_ok_temp_index;
            if (work_mode[mem_ok_temp_index]==3'b100) begin
                read_en_mem = 1;
                write_en_mem = 0;
            end
            else if (work_mode[mem_ok_temp_index]==3'b101) begin
                read_en_mem = 0;
                write_en_mem = 1;
            end
            else begin
                read_en_mem = 0;
                write_en_mem = 0;
            end
            rd_rob_no_mem = rd_relation[mem_ok_temp_index];
        end
    end

    always @(posedge clk) begin
        //非运算指令的算术运算
        if (specific_ok_head != specific_ok_tail) begin
            specific_ok_temp_index = specific_ok_queue[specific_ok_head];
            specific_X = specific_ok_x[specific_ok_head];
            specific_Y = specific_ok_y[specific_ok_head];
            specific_ctrl = specific_ok_ctrl[specific_ok_head];
            specific_save_no = specific_ok_temp_index;
            specific_ok_head = (specific_ok_head+1)%16;
            specific_ready_en = 1;
        end
        else begin
            specific_ready_en = 0;
        end 
    end


    //内容更新
    always @(specific_commit_en or specific_commit_save_no or specific_commit_value) begin
        //注意：此类指令的写回并不能激活其他保留站，只能更新自身保留站条目的状态
        //非算术运算的计算结果写回
        if (specific_commit_en==1) begin
            //更新保留站中等待地址的寄存器状态
            specific_value[specific_commit_save_no] = specific_commit_value;
            specific_ok[specific_commit_save_no] = 1;
            //如果是分支指令写回
            if (work_mode[specific_commit_save_no]==3'b110) begin
                //$display("分支指令跳转地址: ", specific_commit_value);
            end
            else if (work_mode[specific_commit_save_no]==3'b101) begin
                //检查当前的rt是否是就绪状态
                if (rt_ok[specific_commit_save_no]==1) begin
                    //可以访存，将其加入访存就绪队列
                    mem_ok_queue[mem_ok_tail] = specific_commit_save_no;
                    mem_ok_tail = (mem_ok_tail+1)%16;
                end
            end
            else if (work_mode[specific_commit_save_no]==3'b100) begin
                //可以访存，将其加入访存就绪队列
                //$display("LOAD ADDR: ", specific_commit_value);
                mem_ok_queue[mem_ok_tail] = specific_commit_save_no;
                mem_ok_tail = (mem_ok_tail+1)%16;
            end
            else begin
                
            end
        end
    end

    always @(mem_commit_en or mem_commit_save_no or mem_commit_rob_no or mem_commit_value) begin
        if (mem_commit_en==1) begin
            for (i=0; i<16; i++) begin
                mem_temp_map_no = map[mem_commit_rob_no][i];
                //更新保留站的值
                if (i < mapLen[mem_commit_rob_no]) begin
                    //对比rs和rt关联的ROB，如果对应上了，则更新寄存器状态和值
                    if (rs_relation[mem_temp_map_no]==mem_commit_rob_no) begin
                        rs_value[mem_temp_map_no] = mem_commit_value;
                        rs_ok[mem_temp_map_no] = 1;
                    end
                    else if (rt_relation[mem_temp_map_no]==mem_commit_rob_no) begin
                        rt_value[mem_temp_map_no] = mem_commit_value;
                        rt_ok[mem_temp_map_no] = 1;
                    end
                    else begin
                        //对比不上, 不做处理
                    end

                    if (rt_ok[mem_temp_map_no] && rs_ok[mem_temp_map_no]) begin
                        //对于普通运算来说，这个时候可以将运算放入就绪队列
                        if (work_mode[mem_temp_map_no] == 3'b000) begin
                            alu_ok_queue[alu_ok_tail] = mem_temp_map_no;
                            alu_ok_tail = (alu_ok_tail+1)%16;
                        end
                        else if (work_mode[mem_temp_map_no] == 3'b001) begin
                            mul_ok_queue[mul_ok_tail] = mem_temp_map_no;  
                            mul_ok_tail = (mul_ok_tail+1)%16;
                        end
                        else if (work_mode[mem_temp_map_no] == 3'b010) begin
                            div_ok_queue[div_ok_tail] = mem_temp_map_no;
                            div_ok_tail = (div_ok_tail+1)%16;
                        end
                        else if (work_mode[mem_temp_map_no]==3'b100 || work_mode[mem_temp_map_no]==3'b101) begin
                            if (specific_ok[mem_temp_map_no]==1) begin
                                //准备就绪，发射到访存单元
                                mem_ok_queue[mem_ok_tail] = mem_temp_map_no;
                                mem_ok_tail = (mem_ok_tail+1)%16;
                            end
                            else begin
                                //地址单元没有准备好，发射到计算单元
                                specific_ok_queue[specific_ok_tail] = mem_temp_map_no;
                                specific_ok_tail = (specific_ok_tail+1)%16;
                            end
                        end
                    end
                    else if (rs_ok[mem_temp_map_no]) begin
                        if (work_mode[mem_temp_map_no]==3'b100 || work_mode[mem_temp_map_no]==3'b101) begin
                            if (specific_ok[mem_temp_map_no]==0) begin
                                //地址单元没有准备好，发射到计算单元
                                specific_ok_queue[specific_ok_tail] = mem_temp_map_no;
                                specific_ok_tail = (specific_ok_tail+1)%16;
                            end
                        end
                    end
                    else if (rt_ok[mem_temp_map_no]) begin
                        if (work_mode[mem_temp_map_no]==3'b100 || work_mode[mem_temp_map_no]==3'b101) begin
                            if (specific_ok[mem_temp_map_no]==1) begin
                                //准备就绪，发射到访存单元
                                mem_ok_queue[mem_ok_tail] = mem_temp_map_no;
                                mem_ok_tail = (mem_ok_tail+1)%16;
                            end
                        end
                    end
                end
            end
            // //将当前写回的指令所占据的保留站条目释放，此条目加入空闲块队列
            freeQueue[free_tail] = mem_commit_save_no;
            free_tail=(free_tail+1)%32;
            //将长度改成0，冲刷当前映射表条目
            mapLen[mem_commit_rob_no] = 0;
        end
    end


   always @(alu_commit_en or alu_commit_save_no or alu_commit_rob_no or alu_commit_value) begin
        //普通运算写回
        if (alu_commit_en==1) begin
            //如果是分支指令写回，则需要在这个时候判断一下分支结果
            if (work_mode[alu_commit_save_no]==3'b110) begin
                //有符号数
                branch_result = alu_commit_value;
                specific_ok_x[specific_ok_tail] = {{20{1'b0}}, pc[alu_commit_save_no]};
                if (branch_result<0) begin  //bge
                    //不用跳转 pc+4
                    specific_ok_queue[specific_ok_tail] = alu_commit_save_no;
                    specific_ok_y[specific_ok_tail] = 4;
                    specific_ok_ctrl[specific_ok_tail] = 4'b0000;
                    specific_ok_tail = (specific_ok_tail+1)%16;
                end
                else if (branch_result>=0) begin
                    //跳转  pc+imm
                    specific_ok_queue[specific_ok_tail] = alu_commit_save_no;
                    specific_ok_y[specific_ok_tail] = imm[alu_commit_save_no];
                    specific_ok_ctrl[specific_ok_tail] = 4'b0000;
                    specific_ok_tail = (specific_ok_tail+1)%16;
                end
                else begin
                    
                end
            end
            else begin
                for (i=0; i<16; i++) begin
                    alu_temp_map_no = map[alu_commit_rob_no][i];
                    //更新保留站的值
                    if (i < mapLen[alu_commit_rob_no]) begin
                        //对比rs和rt关联的ROB，如果对应上了，则更新寄存器状态和值
                        if (rs_relation[alu_temp_map_no]==alu_commit_rob_no) begin
                            rs_value[alu_temp_map_no] = alu_commit_value;
                            rs_ok[alu_temp_map_no] = 1;
                            //$display("rs-修改保留站: ", alu_temp_map_no, " v1: ", rs_value[alu_temp_map_no], " v2: ", alu_commit_value);
                        end
                        else if (rt_relation[alu_temp_map_no]==alu_commit_rob_no) begin
                            rt_value[alu_temp_map_no] = alu_commit_value;
                            rt_ok[alu_temp_map_no] = 1;
                            //$display("rt-修改保留站: ", alu_temp_map_no, " v1: ", rt_value[alu_temp_map_no], " v2: ", alu_commit_value);
                        end
                        else begin
                            //对比不上, 不做处理
                        end

                        if (rt_ok[alu_temp_map_no] && rs_ok[alu_temp_map_no]) begin
                            //对于普通运算来说，这个时候可以将运算放入就绪队列
                            if (work_mode[alu_temp_map_no] == 3'b000 || work_mode[alu_temp_map_no] == 3'b110) begin
                                //$display("mode: ", work_mode[alu_temp_map_no], " no: ", alu_temp_map_no);
                                //$display("保留站编号: ", alu_temp_map_no, " rt状态: ", rt_value[alu_temp_map_no], " rs状态: ", rs_value[alu_temp_map_no]);
                                alu_ok_queue[alu_ok_tail] = alu_temp_map_no;
                                alu_ok_tail = (alu_ok_tail+1)%16;
                            end
                            else if (work_mode[alu_temp_map_no] == 3'b001) begin
                                mul_ok_queue[mul_ok_tail] = alu_temp_map_no;  
                                mul_ok_tail = (mul_ok_tail+1)%16;
                            end
                            else if (work_mode[alu_temp_map_no] == 3'b010) begin
                                div_ok_queue[div_ok_tail] = alu_temp_map_no;
                                div_ok_tail = (div_ok_tail+1)%16;
                            end
                            else if (work_mode[alu_temp_map_no]==3'b100 || work_mode[alu_temp_map_no]==3'b101) begin
                                if (specific_ok[alu_temp_map_no]==1) begin
                                    //准备就绪，发射到访存单元
                                    mem_ok_queue[mem_ok_tail] = alu_temp_map_no;
                                    mem_ok_tail = (mem_ok_tail+1)%16;
                                end
                                else begin
                                    //地址单元没有准备好，发射到计算单元
                                    specific_ok_queue[specific_ok_tail] = alu_temp_map_no;
                                    specific_ok_tail = (specific_ok_tail+1)%16;
                                end
                            end
                        end
                        else if (rs_ok[alu_temp_map_no]) begin
                            if (work_mode[alu_temp_map_no]==3'b100 || work_mode[alu_temp_map_no]==3'b101) begin
                                if (specific_ok[alu_temp_map_no]==0) begin
                                    specific_ok_x[specific_ok_tail] = rs_value[alu_temp_map_no];
                                    specific_ok_y[specific_ok_tail] = imm[alu_temp_map_no];
                                    specific_ok_ctrl[specific_ok_tail] = alu_ctrl[alu_temp_map_no];
                                    //地址单元没有准备好，发射到计算单元
                                    specific_ok_queue[specific_ok_tail] = alu_temp_map_no;
                                    specific_ok_tail = (specific_ok_tail+1)%16;
                                end
                            end
                        end
                        else if (rt_ok[alu_temp_map_no]) begin
                            if (work_mode[alu_temp_map_no]==3'b100) begin
                                if (specific_ok[alu_temp_map_no]==1) begin
                                    //准备就绪，发射到访存单元
                                    mem_ok_queue[mem_ok_tail] = alu_temp_map_no;
                                    mem_ok_tail = (mem_ok_tail+1)%16;
                                end
                            end
                        end
                    end
                end
                // //将当前写回的指令所占据的保留站条目释放，此条目加入空闲块队列
                freeQueue[free_tail] = alu_commit_save_no;
                free_tail=(free_tail+1)%32;
                //将长度改成0，冲刷当前映射表条目
                mapLen[alu_commit_rob_no] = 0;
            end
        end
   end

   always @(mul_commit_en or mul_commit_save_no or mul_commit_rob_no or mul_commit_value) begin
        //乘法运算写回
        //$display("mul_commit_rob_no: ", mul_commit_rob_no, "mul_commit_value: ", mul_commit_value);
        if (mul_commit_en==1) begin
            for (j=0; j<16; j++) begin
                mul_temp_map_no = map[mul_commit_rob_no][j];
                //更新保留站的值
                if (j < mapLen[mul_commit_rob_no]) begin
                    if (rs_relation[mul_temp_map_no]==mul_commit_rob_no) begin
                        rs_value[mul_temp_map_no] = mul_commit_value;
                        rs_ok[mul_temp_map_no] = 1;
                        //$display("rs-修改保留站: ", mul_temp_map_no, " v1: ", rs_value[mul_temp_map_no], " v2: ", mul_commit_value);
                    end
                    else if (rt_relation[mul_temp_map_no]==mul_commit_rob_no) begin
                        rt_value[mul_temp_map_no] = mul_commit_value;
                        rt_ok[mul_temp_map_no] = 1;
                        //$display("rt-修改保留站: ", mul_temp_map_no, " v1: ", rt_value[mul_temp_map_no], " v2: ", mul_commit_value);
                    end
                    else begin
                        //对比不上, 不做处理
                    end

                    //更新完毕，将两个操作数都达到就绪状态的内容加入就绪队列
                    if (rt_ok[mul_temp_map_no]==1 && rs_ok[mul_temp_map_no]==1) begin
                        //根据所运行模式，加入到不同的队列中
                        if (work_mode[mul_temp_map_no] == 3'b000  || work_mode[alu_temp_map_no] == 3'b110) begin  //普通alu运算
                            alu_ok_queue[alu_ok_tail] = mul_temp_map_no;
                            alu_ok_tail = (alu_ok_tail+1)%16;
                        end
                        else if (work_mode[mul_temp_map_no] == 3'b001) begin  //整数乘法
                            mul_ok_queue[mul_ok_tail] = mul_temp_map_no;  
                            mul_ok_tail = (mul_ok_tail+1)%16;
                        end
                        else if (work_mode[mul_temp_map_no] == 3'b010) begin  //整数除法
                            div_ok_queue[div_ok_tail] = mul_temp_map_no;
                            div_ok_tail = (div_ok_tail+1)%16;
                        end
                        else begin
                            //TODO 后面可能会支持浮点运算之类的
                        end
                    end
                end
            end
            // //将当前写回的指令所占据的保留站条目释放，此条目加入空闲块队列
            freeQueue[free_tail] = mul_commit_save_no;
            free_tail=(free_tail+1)%32;
            //将长度改成0，冲刷当前映射表条目
            mapLen[mul_commit_rob_no] = 0;
        end
   end

   always @(div_commit_en or div_commit_save_no or div_commit_rob_no or div_commit_value) begin
        //除法运算写回
        if (div_commit_en==1) begin
            for (j=0; j<16; j++) begin
                div_temp_map_no = map[div_commit_rob_no][j];
                //更新保留站的值
                if (j < mapLen[div_commit_rob_no]) begin
                    if (rs_relation[div_temp_map_no]==div_commit_rob_no) begin
                        rs_value[div_temp_map_no] = div_commit_value;
                        rs_ok[div_temp_map_no] = 1;
                        //$display("改了rs: ", div_temp_map_no);
                    end
                    else if (rt_relation[div_temp_map_no]==div_commit_rob_no) begin
                        rt_value[div_temp_map_no] = div_commit_value;
                        rt_ok[div_temp_map_no] = 1;
                        //$display("改了rt: ", div_temp_map_no);
                    end
                    else begin
                        //对比不上, 不做处理
                    end
                    //$display("初始状态: ", rt_ok[div_temp_map_no], " -- ", rs_ok[div_temp_map_no], " 模式: ", work_mode[div_temp_map_no]);
                    //更新完毕，将两个操作数都达到就绪状态的内容加入就绪队列
                    if (rt_ok[div_temp_map_no]==1 && rs_ok[div_temp_map_no]==1) begin
                        //根据所运行模式，加入到不同的队列中
                        if (work_mode[div_temp_map_no] == 3'b000  || work_mode[alu_temp_map_no] == 3'b110) begin  //普通alu运算
                            alu_ok_queue[alu_ok_tail] = div_temp_map_no;
                            alu_ok_tail = (alu_ok_tail+1)%16;
                        end
                        else if (work_mode[div_temp_map_no] == 3'b001) begin  //整数乘法
                            mul_ok_queue[mul_ok_tail] = div_temp_map_no;  
                            mul_ok_tail = (mul_ok_tail+1)%16;
                        end
                        else if (work_mode[div_temp_map_no] == 3'b010) begin  //整数除法
                            div_ok_queue[div_ok_tail] = div_temp_map_no;
                            div_ok_tail = (div_ok_tail+1)%16;
                        end
                        else begin
                            //TODO 后面可能会支持浮点运算之类的
                        end
                    end
                end
            end
            // //将当前写回的指令所占据的保留站条目释放，此条目加入空闲块队列
            freeQueue[free_tail] = div_commit_save_no;
            free_tail=(free_tail+1)%32;
            //将长度改成0，冲刷当前映射表条目
            mapLen[div_commit_rob_no] = 0;
        end
   end
endmodule