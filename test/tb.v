`timescale 1ms / 1ms  //定义时间颗粒度
module tb();
    reg rst, clk;     //定义时钟和复位信号
    reg mul_ready_en;
    reg [31:0] mul_X, mul_Y;
    reg [3:0] mul_ctrl;
    reg [4:0] mul_save_no, mul_rd_rob_no;

    wire mul_done;
    wire [4:0] mul_commit_rob_no, mul_commit_save_no;
    wire [31:0] mul_commit_value;


    //规定iverilog的生成文件和制定模块，如果使用其他构建工具，此块内容删掉即可
    initial begin            
        $dumpfile("tb.vcd");        //生成的vcd文件名称
        $dumpvars(0, tb);     //tb模块名称
    end

    //生成时钟信号，每10ns跳变一次
    initial begin
        rst = 0;
        clk = 0;
        forever begin
            #10 clk = ~clk;
        end
    end

    initial begin
        #10 
        mul_ready_en = 1;
        mul_X = 1199;
        mul_Y = 192;
        mul_ctrl = 3'b0000;
        mul_save_no = 0;
        mul_rd_rob_no = 1;
    end

    //乘法器实例化
    wallace mul1(.clk(clk), .rst(), .data_ready(mul_ready_en), .x(mul_X), .y(mul_Y), .ctrl(mul_ctrl), .save_no_in(mul_save_no), .rd_rob_in(mul_rd_rob_no), .done(mul_done), .save_no_out(mul_commit_save_no), .rd_rob_out(mul_commit_rob_no), .result(mul_commit_value));

endmodule