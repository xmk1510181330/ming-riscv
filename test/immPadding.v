/**
 ** 立即数填充模块
 ** 立即数模块需要等待译码器解析出来填充方式控制信号再进行填充
**/

module immPadding (
    input wire [2:0] imm_ctrl,   //立即数填充的控制信号
    input wire [31:0] now_inst,  //当前的指令内容
    output reg [31:0] imm_out    //填充过后的立即数
);

    // 根据不用的指令类型输出不同填充方式的立即数
    always @(*) begin
        case (imm_ctrl)
            3'b001: imm_out[31:0] = {now_inst[31:12], {12{1'b0}}};
            3'b010: imm_out[31:0] = {{12{now_inst[31]}}, now_inst[19:12], now_inst[20], now_inst[30:21], 1'b0};
            3'b011: imm_out[31:0] = {{20{now_inst[31]}}, now_inst[7], now_inst[30:25], now_inst[11:8], 1'b0};
            3'b100: imm_out[31:0] = {{20{now_inst[31]}},now_inst[31:20]};
            3'b101: imm_out[31:0] = {{20{now_inst[31]}},now_inst[31:20]};
            3'b110: imm_out[31:0] = {{20{now_inst[31]}},now_inst[31:25], now_inst[11:7]};
            3'b111: imm_out[31:0] = {{12{now_inst[31]}}, now_inst[31:12]};   //加载立即数
            default: imm_out[31:0] = 32'h00000000;
        endcase
    end

endmodule