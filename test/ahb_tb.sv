`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2023/12/18 17:29:26
// Design Name: 
// Module Name: ahb_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "IF_AHB_paras.vh"

module ahb_tb;

bit           clk;
bit           rst_n;

/* 1. ID branch */
bit           if_jump = 0;
bit [31:0]    prdt_pc_add_op1 = 0;
bit [31:0]    prdt_pc_add_op2 = 0;

/* 2. L/S instr */
bit           d_hready = 1;

/* 3. DIV instr */
bit           div_alu_time = 0;

/* 4. data hazard */
bit           wr_stop = 0;

/* 5. interrupt  */
wire  [31:0] normal_PC;
bit           trap_entry_en = 0;
bit           trap_exit_en = 0;
bit [31:0]    trap_entry_pc = 0;
bit [31:0]    restore_pc = 0;

/* 5. ahb master */
logic         i_hbusreq;
logic [1:0]    i_htrans; /* IDLE, NONSEQ */
logic [31:0]   i_haddr;
logic          i_hwrite;
logic [63:0]   i_hwdata;
logic [2:0]    i_hsize; /* 3'b011 */
logic [2:0]    i_hburst;/* 3'b000 */
logic [2:0]    i_hprot;/* 3'b010 */
logic           i_hgrant;
logic           i_hready;
logic  [1:0]    i_hresp;
logic [63:0]    i_hrdata;

wire [31:0] instr1;
wire instr1_vld;
wire [31:0] instr2;
wire instr2_vld;

always #5 clk = ~clk;

initial begin
    rst_n = 1'b1;
    #20
    rst_n = 1'b0;
    #20
    rst_n = 1'b1;
    #1000 $finish();
end

ITCM_top u_ITCM(
    .clk(clk),
    .rst_n(rst_n),
    
    .i_hsel(1'b1),
    .i_hwrite(1'b0),
    .i_haddr(i_haddr),
    .i_htrans(i_htrans),
    .i_hready(i_hready),
    .i_hresp(i_hresp),
    .i_hrdata(i_hrdata)
    
);


ahb_arbiter u_ahb_arbiter(
        .clk(clk),
        .rst_n(rst_n),
        
        .i_hbusreq(i_hbusreq),
        .i_hgrant(i_hgrant),    
        
        .i_hresp(i_hresp),
        .i_hready(i_hready),
        
        .d_hbusreq(1'b0),
        .d_hgrant(),
        
        .d_hresp('d0),
        .d_hready('d0)    
);

IF2ITCM u_if(
                .clk(clk),
                .rst_n(rst_n),
    
    /* 1. ID branch */
                .if_jump(if_jump),
                .prdt_pc_add_op1(normal_PC),
                .prdt_pc_add_op2('d1),
    
    /* 2. L/S instr */
                .d_hready(d_hready),
    
    /* 3. DIV instr */
                .div_alu_time(div_alu_time),
    
    /* 4. data hazard */
                .wr_stop(wr_stop),
    
    /* 5. interrupt  */
                .normal_PC(normal_PC),
                .trap_entry_en(trap_entry_en),
                .trap_exit_en(trap_exit_en),
                .trap_entry_pc(trap_entry_pc),
                .restore_pc(restore_pc),
    
    /* 5. ahb master */
                .i_hbusreq(i_hbusreq),
                .i_htrans(i_htrans), /* IDLE(), NONSEQ */
                .i_haddr(i_haddr),
                .i_hwrite(i_hwrite),
                .i_hwdata(i_hwdata),
                .i_hsize(i_hsize), /* 3'b011 */
                .i_hburst(i_hburst),/* 3'b000 */
                .i_hprot(i_hprot),/* 3'b010 */
                .i_hgrant(i_hgrant),
                .i_hready(i_hready),
                .i_hresp(i_hresp),
                .i_hrdata(i_hrdata),
    
                .instr1(instr1),
                .instr1_vld(instr1_vld),
                .instr2(instr2),
                .instr2_vld(instr2_vld)   
);

endmodule
