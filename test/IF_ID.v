module IF_ID (
    input wire clk,
    input wire rst,
    input wire [11:0] pc_1_in, pc_2_in,
    input wire [31:0] inst_1_in,
    input wire [31:0] inst_2_in,
    output reg [11:0] pc_1_out, pc_2_out,
    output reg [31:0] inst_1_out,
    output reg [31:0] inst_2_out
);

    always @(posedge clk) begin
        inst_1_out <= inst_1_in;
        inst_2_out <= inst_2_in;

        pc_1_out <= pc_1_in;
        pc_2_out <= pc_2_in;
    end
    
endmodule