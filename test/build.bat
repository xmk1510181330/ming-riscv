@echo off
echo start rtl complier
iverilog -o wave *.v
echo start simulation

echo open gtkwave pic
vvp -n wave -lxt2 
copy tb.vcd tb.lxt

gtkwave tb.vcd

pause