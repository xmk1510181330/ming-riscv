// Stage 1 to Stage 2 Pipeline Registers
reg [4:0] rd_1_mp, rd_2_mp; // Register number mappings
reg rd_1_mp_vld, rd_2_mp_vld; // Valid flags
// 移除next后缀的寄存器定义，因为我们将在流水线寄存器之间使用非阻塞赋值

// Mapping arrays and flags (definition assumed to be correct)
reg [5:0] find_map [0:31];
reg [31:0] find_map_use;
reg [31:0] find_map_ok;

// 第一阶段 - 分配空闲的物理块
always @(posedge clk) begin
    // 为可变寄存器的变化使用非阻塞赋值
    if (rd_1>0 && rd_2>0) begin
        rd_1_mp <= real_free_queue[free_head];
        rd_2_mp <= real_free_queue[free_head+1];
        free_head <= (free_head+2)%64;
        rd_1_mp_vld <= 1;
        rd_2_mp_vld <= 1;
    end
    else if (rd_1>0) begin
        rd_1_mp <= real_free_queue[free_head];
        free_head <= (free_head+1)%64;
        rd_1_mp_vld <= 1;
        rd_2_mp_vld <= 0;       
    end
    else if (rd_2>0) begin
        rd_2_mp <= real_free_queue[free_head];
        free_head <= (free_head+1)%64;
        rd_1_mp_vld <= 0;
        rd_2_mp_vld <= 1;
    end
    else begin
        // 如果两者都无效，不更新寄存器和空闲指针
        rd_1_mp_vld <= 0;
        rd_2_mp_vld <= 0;
    end
end

// 第二阶段 - 设置映射表的内容
always @(posedge clk) begin
    // 使用来自上一阶段的有效信号
    if (rd_1_mp_vld && rd_2_mp_vld) begin // 移除next后缀来使用流水线寄存器的输出
        find_map[rd_1] <= rd_1_mp;
        find_map_use[rd_1] <= 1;
        find_map_ok[rd_1] <= 0;
        find_map[rd_2] <= rd_2_mp;
        find_map_use[rd_2] <= 1;
        find_map_ok[rd_2] <= 0;
    end
    else if (rd_1_mp_vld) begin
        find_map[rd_1] <= rd_1_mp;
        find_map_use[rd_1] <= 1;
        find_map_ok[rd_1] <= 0;
    end
    else if (rd_2_mp_vld) begin
        find_map[rd_2] <= rd_2_mp;
        find_map_use[rd_2] <= 1;
        find_map_ok[rd_2] <= 0;
    end
    // 注意：我们省略了else部分，通常这里应当处理其他情况或保持当前状态
end