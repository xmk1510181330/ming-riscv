module OP_SCH (
    input wire clk, rst,
    input wire [11:0] pc_1_in, pc_2_in,
    input wire [31:0] imm_1_in, imm_2_in,
    input wire opt_x_1_ready_in, opt_y_1_ready_in, opt_x_2_ready_in, opt_y_2_ready_in,
    input wire [31:0] opt_x_1_value_in, opt_y_1_value_in, opt_x_2_value_in, opt_y_2_value_in,
    input wire [4:0] opt_x_1_rob_in, opt_y_1_rob_in, opt_x_2_rob_in, opt_y_2_rob_in,
    input wire rs_1_ready_in, rt_1_ready_in, rs_2_ready_in, rt_2_ready_in,
    input wire [31:0] rs_1_value_in, rt_1_value_in, rs_2_value_in, rt_2_value_in,
    input wire [4:0] rs_1_rob_in, rt_1_rob_in, rs_2_rob_in, rt_2_rob_in, rd_1_rob_in, rd_2_rob_in,
    input wire [2:0] work_mode_1_in, work_mode_2_in,
    input wire [3:0] alu_ctrl_1_in, alu_ctrl_2_in, complex_ctrl_1_in, complex_ctrl_2_in,
    input wire alu_commit_en_in, mul_commit_en_in, div_commit_en_in, specific_commit_en_in, mem_commit_en_in,
    input wire [4:0] alu_commit_save_no_in, mul_commit_save_no_in, div_commit_save_no_in, specific_commit_save_no_in, mem_commit_save_no_in,
    input wire [4:0] alu_commit_rob_no_in, mul_commit_rob_no_in, div_commit_rob_no_in, mem_commit_rob_no_in,
    input wire [31:0] alu_commit_value_in, mul_commit_value_in, div_commit_value_in, specific_commit_value_in, mem_commit_value_in,
    input wire [2:0] dm_rd_ctrl_1_in, dm_rd_ctrl_2_in,
    input wire [1:0] dm_wr_ctrl_1_in, dm_wr_ctrl_2_in,
    output reg [11:0] pc_1_out, pc_2_out,
    output reg [31:0] imm_1_out, imm_2_out,
    output reg rs_1_ready_out, rt_1_ready_out, rs_2_ready_out, rt_2_ready_out,
    output reg [31:0] rs_1_value_out, rt_1_value_out, rs_2_value_out, rt_2_value_out,
    output reg [4:0] rs_1_rob_out, rt_1_rob_out, rs_2_rob_out, rt_2_rob_out, rd_1_rob_out, rd_2_rob_out,
    output reg [2:0] work_mode_1_out, work_mode_2_out, 
    output reg [3:0] alu_ctrl_1_out, alu_ctrl_2_out, complex_ctrl_1_out, complex_ctrl_2_out,
    output reg save_en_1, save_en_2,  //保留站使能信号
    output reg alu_commit_en_out, mul_commit_en_out, div_commit_en_out, specific_commit_en_out, mem_commit_en_out,
    output reg [4:0] alu_commit_save_no_out, mul_commit_save_no_out, div_commit_save_no_out, specific_commit_save_no_out, mem_commit_save_no_out,
    output reg [4:0] alu_commit_rob_no_out, mul_commit_rob_no_out, div_commit_rob_no_out, mem_commit_rob_no_out,
    output reg [31:0] alu_commit_value_out, mul_commit_value_out, div_commit_value_out, specific_commit_value_out, mem_commit_value_out,
    output reg [2:0] dm_rd_ctrl_1_out, dm_rd_ctrl_2_out,
    output reg [1:0] dm_wr_ctrl_1_out, dm_wr_ctrl_2_out
);
    
    always @(posedge clk) begin
        rs_1_ready_out <= rs_1_ready_in;
        rt_1_ready_out <= rt_1_ready_in;
        rs_2_ready_out <= rs_2_ready_in;
        rt_2_ready_out <= rt_2_ready_in;
        rs_1_value_out <= rs_1_value_in;
        rt_1_value_out <= rt_1_value_in;
        rs_2_value_out <= rs_2_value_in;
        rt_2_value_out <= rt_2_value_in;
        rs_1_rob_out <= rs_1_rob_in;
        rt_1_rob_out <= rt_1_rob_in;
        rs_2_rob_out <= rs_2_rob_in;
        rt_2_rob_out <= rt_2_rob_in;
        rd_1_rob_out <= rd_1_rob_in;
        rd_2_rob_out <= rd_2_rob_in;
        alu_ctrl_1_out <= alu_ctrl_1_in;
        alu_ctrl_2_out <= alu_ctrl_2_in;
        complex_ctrl_1_out <= complex_ctrl_1_in;
        complex_ctrl_2_out <= complex_ctrl_2_in;
        work_mode_1_out <= work_mode_1_in;
        work_mode_2_out <= work_mode_2_in;
        alu_commit_en_out <= alu_commit_en_in;
        mul_commit_en_out <= mul_commit_en_in;
        div_commit_en_out <= div_commit_en_in;
        specific_commit_en_out <= specific_commit_en_in;
        mem_commit_en_out <= mem_commit_en_in;
        alu_commit_rob_no_out <= alu_commit_rob_no_in;
        mul_commit_rob_no_out <= mul_commit_rob_no_in;
        div_commit_rob_no_out <= div_commit_rob_no_in;
        mem_commit_rob_no_out <= mem_commit_rob_no_in;
        alu_commit_value_out <= alu_commit_value_in;
        mul_commit_value_out <= mul_commit_value_in;
        div_commit_value_out <= div_commit_value_in;
        specific_commit_value_out <= specific_commit_value_in;
        mem_commit_value_out <= mem_commit_value_in;
        alu_commit_save_no_out <= alu_commit_save_no_in;
        mul_commit_save_no_out <= mul_commit_save_no_in;
        div_commit_save_no_out <= div_commit_save_no_in;
        specific_commit_save_no_out <= specific_commit_save_no_in;
        mem_commit_save_no_out <= mem_commit_save_no_in;

        pc_1_out <= pc_1_in;
        pc_2_out <= pc_2_in;

        imm_1_out <= imm_1_in;
        imm_2_out <= imm_2_in;

        dm_rd_ctrl_1_out <= dm_rd_ctrl_1_in;
        dm_rd_ctrl_2_out <= dm_rd_ctrl_2_in;
        dm_wr_ctrl_1_out <= dm_wr_ctrl_1_in;
        dm_wr_ctrl_2_out <= dm_wr_ctrl_2_in;


        //如果是运算指令，则需要开启使能信号，保存到保留站
        if (work_mode_1_in==3'b000 || work_mode_1_in==3'b001 || work_mode_1_in==3'b010 || work_mode_1_in==3'b011 || work_mode_1_in==3'b100 || work_mode_1_in==3'b101 || work_mode_1_in==3'b110) begin
            save_en_1 <= 1;
        end
        else begin
            save_en_1 <= 0;
        end

        if (work_mode_2_in==3'b000 || work_mode_2_in==3'b001 || work_mode_2_in==3'b010 || work_mode_2_in==3'b011 || work_mode_2_in==3'b100 || work_mode_2_in==3'b101 || work_mode_2_in==3'b110) begin
            save_en_2 <= 1;
        end
        else begin
            save_en_2 <= 0;
        end
    end
endmodule