module ID_OP (
    input wire clk,
    input wire [11:0] pc_1_in, pc_2_in,
    input wire [4:0] reg_rs_1_in, reg_rt_1_in, reg_rd_1_in,
    input wire rob_write_en_1_in,
    input wire [1:0] rob_write_sel_1_in,
    input wire alu_a_sel_1_in, alu_b_sel_1_in,
    input wire [2:0] work_mode_1_in,
    input wire [3:0] alu_ctrl_1_in, complex_ctrl_1_in,
    input wire [2:0] dm_rd_ctrl_1_in,
    input wire [1:0] dm_wr_ctrl_1_in,
    input wire rob_save_direct_ctrl_1,
    input wire [31:0] imm_out_1_in,
    input wire [4:0] reg_rs_2_in, reg_rt_2_in, reg_rd_2_in,
    input wire rob_write_en_2_in,
    input wire [1:0] rob_write_sel_2_in,
    input wire alu_a_sel_2_in, alu_b_sel_2_in,
    input wire [2:0] work_mode_2_in,
    input wire [3:0] alu_ctrl_2_in, complex_ctrl_2_in,
    input wire [2:0] dm_rd_ctrl_2_in,
    input wire [1:0] dm_wr_ctrl_2_in,
    input wire rob_save_direct_ctrl_2,
    input wire [31:0] imm_out_2_in,
    input wire alu_commit_en_in, mul_commit_en_in, div_commit_en_in, mem_commit_en_in,
    input wire [4:0] alu_commit_rob_no_in, mul_commit_rob_no_in, div_commit_rob_no_in, mem_commit_rob_no_in,
    input wire [31:0] alu_commit_value_in, mul_commit_value_in, div_commit_value_in, mem_commit_value_in,
    output reg [11:0] pc_1_out, pc_2_out,
    output reg [4:0] reg_rs_1_out, reg_rt_1_out, reg_rd_1_out,
    output reg rob_write_en_1_out,
    output reg [1:0] rob_write_sel_1_out,
    output reg alu_a_sel_1_out, alu_b_sel_1_out,
    output reg [2:0] work_mode_1_out,
    output reg [3:0] alu_ctrl_1_out, complex_ctrl_1_out,
    output reg [2:0] dm_rd_ctrl_1_out,
    output reg [1:0] dm_wr_ctrl_1_out,
    output reg rob_save_direct_ctrl_1_op,
    output reg [31:0] imm_out_1_out,
    output reg [4:0] reg_rs_2_out, reg_rt_2_out, reg_rd_2_out,
    output reg rob_write_en_2_out,
    output reg [1:0] rob_write_sel_2_out,
    output reg alu_a_sel_2_out, alu_b_sel_2_out,
    output reg [2:0] work_mode_2_out,
    output reg [3:0] alu_ctrl_2_out, complex_ctrl_2_out,
    output reg [2:0] dm_rd_ctrl_2_out,
    output reg [1:0] dm_wr_ctrl_2_out,
    output reg rob_save_direct_ctrl_2_op,
    output reg [31:0] imm_out_2_out,
    output reg alu_commit_en_out, mul_commit_en_out, div_commit_en_out, mem_commit_en_out,
    output reg [4:0] alu_commit_rob_no_out, mul_commit_rob_no_out, div_commit_rob_no_out, mem_commit_rob_no_out,
    output reg [31:0] alu_commit_value_out, mul_commit_value_out, div_commit_value_out, mem_commit_value_out
);

    always @(posedge clk) begin
        reg_rs_1_out <= reg_rs_1_in;
        reg_rt_1_out <= reg_rt_1_in;
        reg_rd_1_out <= reg_rd_1_in;
        rob_write_en_1_out <= rob_write_en_1_in;
        rob_write_sel_1_out <= rob_write_sel_1_in;
        alu_a_sel_1_out <= alu_a_sel_1_in;
        alu_b_sel_1_out <= alu_b_sel_1_in;
        work_mode_1_out <= work_mode_1_in;
        alu_ctrl_1_out <= alu_ctrl_1_in;
        complex_ctrl_1_out <= complex_ctrl_1_in;
        dm_rd_ctrl_1_out <= dm_rd_ctrl_1_in;
        dm_wr_ctrl_1_out <= dm_wr_ctrl_1_in;
        rob_save_direct_ctrl_1_op <= rob_save_direct_ctrl_1;
        imm_out_1_out <= imm_out_1_in;

        reg_rs_2_out <= reg_rs_2_in;
        reg_rt_2_out <= reg_rt_2_in;
        reg_rd_2_out <= reg_rd_2_in;
        rob_write_en_2_out <= rob_write_en_2_in;
        rob_write_sel_2_out <= rob_write_sel_2_in;
        alu_a_sel_2_out <= alu_a_sel_2_in;
        alu_b_sel_2_out <= alu_b_sel_2_in;
        work_mode_2_out <= work_mode_2_in;
        alu_ctrl_2_out <= alu_ctrl_2_in;
        complex_ctrl_2_out <= complex_ctrl_2_in;
        dm_rd_ctrl_2_out <= dm_rd_ctrl_2_in;
        dm_wr_ctrl_2_out <= dm_wr_ctrl_2_in;
        rob_save_direct_ctrl_2_op <= rob_save_direct_ctrl_2;
        imm_out_2_out <= imm_out_2_in;

        alu_commit_en_out <= alu_commit_en_in;
        mul_commit_en_out <= mul_commit_en_in;
        div_commit_en_out <= div_commit_en_in;
        mem_commit_en_out <= mem_commit_en_in;
        alu_commit_rob_no_out <= alu_commit_rob_no_in;
        mul_commit_rob_no_out <= mul_commit_rob_no_in;
        div_commit_rob_no_out <= div_commit_rob_no_in;
        mem_commit_rob_no_out <= mem_commit_rob_no_in;
        alu_commit_value_out <= alu_commit_value_in;
        mul_commit_value_out <= mul_commit_value_in;
        div_commit_value_out <= div_commit_value_in;
        div_commit_value_out <= div_commit_value_in;

        pc_1_out <= pc_1_in;
        pc_2_out <= pc_2_in;
    end
    
endmodule