/**
 ** 16位华莱士树，整个华莱士树由64个此单元级联形成
**/
module wallaceItem (
    input wire [15:0] x,             //要运算的16位数
    input wire [13:0] carry_pre,     //前一级传递的进位
    output wire sum,                 //输出的两位结果
    output wire carry,               
    output wire [13:0] carry_next    //传递给后一级的进位
);

    wire [4:0] level1_sum;
    fulladdr adder11(.a(x[1]), .b(x[2]), .carry_in(x[3]), .s(level1_sum[0]), .carry_out(carry_next[4]));
    fulladdr adder12(.a(x[4]), .b(x[5]), .carry_in(x[6]), .s(level1_sum[1]), .carry_out(carry_next[3]));
    fulladdr adder13(.a(x[7]), .b(x[8]), .carry_in(x[9]), .s(level1_sum[2]), .carry_out(carry_next[2]));
    fulladdr adder14(.a(x[10]), .b(x[11]), .carry_in(x[12]), .s(level1_sum[3]), .carry_out(carry_next[1]));
    fulladdr adder15(.a(x[13]), .b(x[14]), .carry_in(x[15]), .s(level1_sum[4]), .carry_out(carry_next[0]));
    
    wire [2:0] level2_sum;
    fulladdr adder21(.a(carry_pre[0]), .b(carry_pre[1]), .carry_in(carry_pre[2]), .s(level2_sum[0]), .carry_out(carry_next[7]));
    fulladdr adder22(.a(x[0]), .b(level1_sum[0]), .carry_in(level1_sum[1]), .s(level2_sum[1]), .carry_out(carry_next[6]));
    fulladdr adder23(.a(level1_sum[2]), .b(level1_sum[3]), .carry_in(level1_sum[4]), .s(level2_sum[2]), .carry_out(carry_next[5]));

    wire [2:0] level3_sum;
    halfaddr adder31(.a(carry_pre[6]), .b(carry_pre[7]), .s(level3_sum[0]), .carry_out(carry_next[10]));
    fulladdr adder32(.a(carry_pre[3]), .b(carry_pre[4]), .carry_in(carry_pre[5]), .s(level3_sum[1]), .carry_out(carry_next[9]));
    fulladdr adder33(.a(level2_sum[0]), .b(level2_sum[1]), .carry_in(level2_sum[2]), .s(level3_sum[2]), .carry_out(carry_next[8]));

    wire [1:0] level4_sum;
    fulladdr adder41(.a(carry_pre[8]), .b(carry_pre[9]), .carry_in(carry_pre[10]), .s(level4_sum[0]), .carry_out(carry_next[12]));
    fulladdr adder42(.a(level3_sum[0]), .b(level3_sum[1]), .carry_in(level3_sum[2]), .s(level4_sum[1]), .carry_out(carry_next[11]));

    wire level5_sum;
    fulladdr adder51(.a(carry_pre[11]), .b(level4_sum[0]), .carry_in(level4_sum[1]), .s(level5_sum), .carry_out(carry_next[13]));

    fulladdr adder61(.a(level5_sum), .b(carry_pre[12]), .carry_in(carry_pre[13]), .s(sum), .carry_out(carry));
endmodule