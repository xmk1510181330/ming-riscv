`timescale 1ms / 1ms  //定义时间颗粒度
module tb();
    reg rst, clk;     //定义时钟和复位信号
    reg [11:0] pc_1, pc_2;
    wire [11:0] pc_1_IF, pc_2_IF, pc_1_ID, pc_2_ID, pc_1_OP, pc_2_OP, pc_1_SCH, pc_2_SCH, pc_1_EXE, pc_2_EXE;
    wire [31:0] X, Y;
    reg [4:0] rob_no_in;
    wire [31:0] result;
    wire [4:0] rob_no_out;
    //指令存储器中读取到两条指令
    wire [31:0] inst_1, inst_2, inst_1_id, inst_2_id;
    wire [31:0] share_buffer;

    //规定iverilog的生成文件和制定模块，如果使用其他构建工具，此块内容删掉即可
    initial begin            
        $dumpfile("tb.vcd");        //生成的vcd文件名称
        $dumpvars(0, tb);     //tb模块名称
    end

    //生成时钟信号，每10ns跳变一次
    initial begin
        rst = 0;
        clk = 0;
        forever begin
            #10 clk = ~clk;
        end
    end

    //在开始给一个复位信号
    initial begin
        #10
        rst = 1;

        #20 
        rst = 0;
    end

    // initial begin
    //     //pc累加
    //     #10
    //     pc_1 = 0;
    //     pc_2 = 0;
    //     forever begin
    //         #20
    //         pc_1 = pc_1 + 2;
    //         pc_2 = pc_2 + 2;
    //     end
    // end

    initial begin
        #30
        //生成PC信号
        pc_1 = 0;
        pc_2 = 1;
        forever begin
            #20
            pc_1 = pc_1 + 2;
            pc_2 = pc_2 + 2;
        end
    end

    //操作数的信号
    wire opt_x_1_ready, opt_y_1_ready, opt_x_2_ready, opt_y_2_ready, opt_x_1_ready_sch, opt_y_1_ready_sch, opt_x_2_ready_sch, opt_y_2_ready_sch;
    wire [31:0] opt_x_1_value, opt_y_1_value, opt_x_2_value, opt_y_2_value, opt_x_1_value_sch, opt_y_1_value_sch, opt_x_2_value_sch, opt_y_2_value_sch;
    wire [4:0] opt_x_1_rob, opt_y_1_rob, opt_x_2_rob, opt_y_2_rob, opt_x_1_rob_sch, opt_y_1_rob_sch, opt_x_2_rob_sch, opt_y_2_rob_sch;
    //原始寄存器号和映射过后的寄存器号
    wire [4:0] rs_1, rs_2, rt_1, rt_2, rd_1, rd_2, rs_1_op, rs_2_op, rt_1_op, rt_2_op, rd_1_op, rd_2_op;
    wire [5:0] rs_1_mp, rs_2_mp, rt_1_mp, rt_2_mp, rd_1_mp, rd_2_mp;
    //寄存器是否准备好的的信号
    wire rs_1_ready, rt_1_ready, rs_2_ready, rt_2_ready, rs_1_ready_sch, rt_1_ready_sch, rs_2_ready_sch, rt_2_ready_sch;
    //寄存器的值
    wire [31:0] rs_1_value, rt_1_value, rs_2_value, rt_2_value, rs_1_value_sch, rt_1_value_sch, rs_2_value_sch, rt_2_value_sch;
    //逻辑寄存器号
    wire [4:0] rd_1_logic, rd_2_logic;
    //rob写入使能
    wire rob_write_en_1, rob_write_en_2, rob_write_en_1_op, rob_write_en_2_op;
    wire [1:0] rob_write_sel_1, rob_write_sel_2, rob_write_sel_1_op, rob_write_sel_2_op;

    //运算器的控制信号
    wire [3:0] alu_ctrl_1, alu_ctrl_2, complex_ctrl_1, complex_ctrl_2, alu_ctrl_1_op, alu_ctrl_2_op, complex_ctrl_1_op, complex_ctrl_2_op, alu_ctrl_1_sch, alu_ctrl_2_sch, complex_ctrl_1_sch, complex_ctrl_2_sch;
    //运算器数据选择信号
    wire alu_a_sel_1, alu_b_sel_1, alu_a_sel_2, alu_b_sel_2, alu_a_sel_1_id, alu_b_sel_1_id, alu_a_sel_2_id, alu_b_sel_2_id, alu_a_sel_1_op, alu_b_sel_1_op, alu_a_sel_2_op, alu_b_sel_2_op;
    //运算模式信号
    wire [2:0] work_mode_1, work_mode_2, work_mode_1_op, work_mode_2_op, work_mode_1_sch, work_mode_2_sch; 
    //立即数扩充后的输出
    wire [31:0] imm_out_1, imm_out_2;
    //源寄存器对应的ROB号
    wire [4:0] rs_1_rob, rt_1_rob, rs_2_rob, rt_2_rob, rd_1_rob, rd_2_rob, rs_1_rob_sch, rt_1_rob_sch, rs_2_rob_sch, rt_2_rob_sch, rd_1_rob_sch, rd_2_rob_sch;
    //立即数直接写回ROB的控制信号
    wire rob_save_direct_ctrl_1, rob_save_direct_ctrl_2, rob_save_direct_ctrl_1_op, rob_save_direct_ctrl_2_op;
    //数据存储器的控制信号
    wire [2:0] dm_rd_ctrl_1, dm_rd_ctrl_2, dm_rd_ctrl_1_op, dm_rd_ctrl_2_op, dm_rd_ctrl_1_sch, dm_rd_ctrl_1_mem, dm_rd_ctrl_2_sch, dm_rd_ctrl_2_mem;
    wire [1:0] dm_wr_ctrl_1, dm_wr_ctrl_2, dm_wr_ctrl_1_op, dm_wr_ctrl_2_op, dm_wr_ctrl_1_sch, dm_wr_ctrl_1_mem, dm_wr_ctrl_2_sch, dm_wr_ctrl_2_mem;

    //立即数的输出信号
    wire [31:0] imm_out_1_op, imm_out_2_op, imm_out_1_sch, imm_out_2_sch;

    //保留站输出的运算单元控制信号
    wire [4:0] result_rob_no, result_rob_no_exe, result_rob_no_alu, result_rob_no_mul, result_rob_no_div;             //结果要写回的ROB
    wire [3:0] common_ctrl, common_ctrl_exe;         //对于运算单元要输出的结果
    wire [31:0] alu_X, alu_X_exe;
    wire [31:0] alu_Y, alu_Y_exe;
    wire alu_ready_en, alu_ready_en_exe;
    wire [3:0] specific_ctrl, specific_ctrl_exe;
    wire [31:0] specific_X, specific_X_exe;
    wire [31:0] specific_Y, specific_Y_exe;
    wire specific_ready_en, specific_ready_en_exe;
    wire [3:0] mul_ctrl, mul_ctrl_exe;
    wire [31:0] mul_X, mul_X_exe;
    wire [31:0] mul_Y, mul_Y_exe;
    wire mul_ready_en, mul_ready_en_exe;
    wire [3:0] div_ctrl, div_ctrl_exe;
    wire [31:0] div_X, div_X_exe;
    wire [31:0] div_Y, div_Y_exe;
    wire div_ready_en, div_ready_en_exe;
    wire [31:0] alu_result, mul_result, div_result, specific_result;
    wire [4:0] alu_save_no, mul_save_no, div_save_no, specific_save_no;

    //访存需要用到的一些信号
    wire read_en_mem, write_en_mem;             //RAM读写使能信号
    wire [2:0] dm_rd_ctrl_mem;
    wire [1:0] dm_wr_ctrl_mem;
    wire [31:0] dm_addr_mem, dm_din_mem;
    wire [4:0] save_no_mem;
    wire [4:0] rd_rob_no_mem;

    //保存到保留站的使能信号
    wire save_en_1, save_en_2;

    //输出到运算器的目标寄存器地址
    wire [4:0] alu_rd_rob_no, mul_rd_rob_no, div_rd_rob_no, alu_rd_rob_no_exe, mul_rd_rob_no_exe, div_rd_rob_no_exe;

    //运算完成标志位
    wire mem_done, alu_done, specific_done, mul_done, div_done, alu_done_sch, specific_done_sch, mem_done_sch, mul_done_sch, div_done_sch, alu_done_op, mul_done_op, div_done_op, mem_done_op;
    wire [4:0] alu_commit_rob_no, mem_commit_rob_no, mul_commit_rob_no, div_commit_rob_no, alu_commit_rob_no_sch, mem_commit_rob_no_sch, mul_commit_rob_no_sch, div_commit_rob_no_sch, alu_commit_rob_no_op, mul_commit_rob_no_op, div_commit_rob_no_op, mem_commit_rob_no_op;
    wire [31:0] alu_commit_value, specific_commit_value, mem_commit_value, mul_commit_value, div_commit_value, alu_commit_value_sch, specific_commit_value_sch, mem_commit_value_sch, mul_commit_value_sch, div_commit_value_sch, alu_commit_value_op, mul_commit_value_op, div_commit_value_op, mem_commit_value_op;
    wire [4:0] alu_commit_save_no, specific_commit_save_no, mem_commit_save_no, mul_commit_save_no, div_commit_save_no, alu_commit_save_no_sch, specific_commit_save_no_sch, mem_commit_save_no_sch, mul_commit_save_no_sch, div_commit_save_no_sch, mem_commit_save_no_op;

    //测试信号
    wire load_over;

    //规定时钟总长
    initial
        #5000 $finish;

    // always @(posedge load_over) begin
    //     //指令加载完毕后, 重置一下PC
    //     pc_1 = -2;
    //     pc_2 = -1;
    // end

    //实例化一个测试用的指令存储器
    //instMemery insts(.clk(clk), .pc_1(), .pc_2(), .inst_one(inst_1), .inst_two(inst_2));
    IF if1(.clk(clk), .rst(rst), .pc_1(pc_1), .pc_2(pc_2), .load_over(load_over), .pc_1_out(pc_1_IF), .pc_2_out(pc_2_IF), .inst_1(inst_1), .inst_2(inst_2));

    IF_ID if_id1(.clk(clk), .rst(), .pc_1_in(pc_1_IF), .pc_2_in(pc_2_IF), .inst_1_in(inst_1), .inst_2_in(inst_2), .pc_1_out(pc_1_ID), .pc_2_out(pc_2_ID), .inst_1_out(inst_1_id), .inst_2_out(inst_2_id));

    ID id1(.inst_1(inst_1_id), .inst_2(inst_2_id),
           .reg_rs_1(rs_1), .reg_rt_1(rt_1), .reg_rd_1(rd_1),
           .rob_write_en_1(rob_write_en_1), .rob_write_sel_1(rob_write_sel_1),
           .alu_a_sel_1(alu_a_sel_1), .alu_b_sel_1(alu_b_sel_1), .work_mode_1(work_mode_1), .alu_ctrl_1(alu_ctrl_1), .complex_ctrl_1(complex_ctrl_1),
           .dm_rd_ctrl_1(dm_rd_ctrl_1), .dm_wr_ctrl_1(dm_wr_ctrl_1), .rob_save_direct_ctrl_1(rob_save_direct_ctrl_1), .imm_out_1(imm_out_1),
           .reg_rs_2(rs_2), .reg_rt_2(rt_2), .reg_rd_2(rd_2),
           .rob_write_en_2(rob_write_en_2), .rob_write_sel_2(rob_write_sel_2),
           .alu_a_sel_2(alu_a_sel_2), .alu_b_sel_2(alu_b_sel_2), .work_mode_2(work_mode_2), .alu_ctrl_2(alu_ctrl_2), .complex_ctrl_2(complex_ctrl_2),
           .dm_rd_ctrl_2(dm_rd_ctrl_2), .dm_wr_ctrl_2(dm_wr_ctrl_2), .rob_save_direct_ctrl_2(rob_save_direct_ctrl_2), .imm_out_2(imm_out_2));

    ID_OP id_op2(.clk(clk),
        .pc_1_in(pc_1_ID), .pc_2_in(pc_2_ID),
        .reg_rs_1_in(rs_1), .reg_rt_1_in(rt_1), .reg_rd_1_in(rd_1),
        .rob_write_en_1_in(rob_write_en_1), .rob_write_sel_1_in(rob_write_sel_1),
        .alu_a_sel_1_in(alu_a_sel_1), .alu_b_sel_1_in(alu_b_sel_1),
        .work_mode_1_in(work_mode_1), .alu_ctrl_1_in(alu_ctrl_1), .complex_ctrl_1_in(complex_ctrl_1),
        .dm_rd_ctrl_1_in(dm_rd_ctrl_1), .dm_wr_ctrl_1_in(dm_wr_ctrl_1),
        .rob_save_direct_ctrl_1(rob_save_direct_ctrl_1),
        .imm_out_1_in(imm_out_1), 
        .reg_rs_2_in(rs_2), .reg_rt_2_in(rt_2), .reg_rd_2_in(rd_2),
        .rob_write_en_2_in(rob_write_en_2), .rob_write_sel_2_in(rob_write_sel_2),
        .alu_a_sel_2_in(alu_a_sel_2), .alu_b_sel_2_in(alu_b_sel_2),
        .work_mode_2_in(work_mode_2), .alu_ctrl_2_in(alu_ctrl_2), .complex_ctrl_2_in(complex_ctrl_2),
        .dm_rd_ctrl_2_in(dm_rd_ctrl_2), .dm_wr_ctrl_2_in(dm_wr_ctrl_2),
        .rob_save_direct_ctrl_2(rob_save_direct_ctrl_2),
        .imm_out_2_in(imm_out_2),
        .alu_commit_en_in(alu_done), .mul_commit_en_in(mul_done), .div_commit_en_in(div_done), .mem_commit_en_in(mem_done),
        .alu_commit_rob_no_in(alu_commit_rob_no), .mul_commit_rob_no_in(mul_commit_rob_no), .div_commit_rob_no_in(div_commit_rob_no), .mem_commit_rob_no_in(mem_commit_rob_no),
        .alu_commit_value_in(alu_commit_value), .mul_commit_value_in(mul_commit_value), .div_commit_value_in(div_commit_value), .mem_commit_value_in(mem_commit_value),
        .pc_1_out(pc_1_OP), .pc_2_out(pc_2_OP),
        .reg_rs_1_out(rs_1_op), .reg_rt_1_out(rt_1_op), .reg_rd_1_out(rd_1_op),
        .rob_write_en_1_out(rob_write_en_1_op), .rob_write_sel_1_out(rob_write_sel_1_op),
        .alu_a_sel_1_out(alu_a_sel_1_op), .alu_b_sel_1_out(alu_b_sel_1_op),
        .work_mode_1_out(work_mode_1_op), .alu_ctrl_1_out(alu_ctrl_1_op), .complex_ctrl_1_out(complex_ctrl_1_op),
        .dm_rd_ctrl_1_out(dm_rd_ctrl_1_op), .dm_wr_ctrl_1_out(dm_wr_ctrl_1_op),
        .rob_save_direct_ctrl_1_op(rob_save_direct_ctrl_1_op),
        .imm_out_1_out(imm_out_1_op), 
        .reg_rs_2_out(rs_2_op), .reg_rt_2_out(rt_2_op), .reg_rd_2_out(rd_2_op),
        .rob_write_en_2_out(rob_write_en_2_op), .rob_write_sel_2_out(rob_write_sel_2_op),
        .alu_a_sel_2_out(alu_a_sel_2_op), .alu_b_sel_2_out(alu_b_sel_2_op),
        .work_mode_2_out(work_mode_2_op), .alu_ctrl_2_out(alu_ctrl_2_op), .complex_ctrl_2_out(complex_ctrl_2_op),
        .dm_rd_ctrl_2_out(dm_rd_ctrl_2_op), .dm_wr_ctrl_2_out(dm_wr_ctrl_2_op),
        .rob_save_direct_ctrl_2_op(rob_save_direct_ctrl_2_op),
        .imm_out_2_out(imm_out_2_op),
        .alu_commit_en_out(alu_done_op), .mul_commit_en_out(mul_done_op), .div_commit_en_out(div_done_op), .mem_commit_en_out(mem_done_op),
        .alu_commit_rob_no_out(alu_commit_rob_no_op), .mul_commit_rob_no_out(mul_commit_rob_no_op), .div_commit_rob_no_out(div_commit_rob_no_op), .mem_commit_rob_no_out(mem_commit_rob_no_op),
        .alu_commit_value_out(alu_commit_value_op), .mul_commit_value_out(mul_commit_value_op), .div_commit_value_out(div_commit_value_op), .mem_commit_value_out(mem_commit_value_op));

    OP op1( .clk(clk),
            .rs_1(rs_1_op), .rt_1(rt_1_op), .rd_1(rd_1_op), .rs_2(rs_2_op), .rt_2(rt_2_op), .rd_2(rd_2_op),
            .rob_write_en_1(rob_write_en_1_op), .rob_write_en_2(rob_write_en_2_op), .rob_write_sel_1(rob_write_sel_1_op), .rob_write_sel_2(rob_write_sel_2_op),
            .rob_save_direct_ctrl_1(rob_save_direct_ctrl_1_op), .rob_save_direct_ctrl_2(rob_save_direct_ctrl_2_op),
            .imm_1(imm_out_1_op), .imm_2(imm_out_2_op),
            .alu_a_sel_1(alu_a_sel_1_op), .alu_b_sel_1(alu_b_sel_1_op), .alu_a_sel_2(alu_a_sel_2_op), .alu_b_sel_2(alu_b_sel_2_op),
            .alu_commit_en(alu_done_op), .mul_commit_en(mul_done_op), .div_commit_en(div_done_op), .mem_commit_en(mem_done_op),
            .alu_commit_rob_no(alu_commit_rob_no_op), .mul_commit_rob_no(mul_commit_rob_no_op), .div_commit_rob_no(div_commit_rob_no_op), .mem_commit_rob_no(mem_commit_rob_no_op),
            .alu_commit_value(alu_commit_value_op), .mul_commit_value(mul_commit_value_op), .div_commit_value(div_commit_value_op), .mem_commit_value(mem_commit_value_op),
            .opt_x_1_ready(opt_x_1_ready), .opt_y_1_ready(opt_y_1_ready), .opt_x_2_ready(opt_x_2_ready), .opt_y_2_ready(opt_y_2_ready),
            .opt_x_1_value(opt_x_1_value), .opt_y_1_value(opt_y_1_value), .opt_x_2_value(opt_x_2_value), .opt_y_2_value(opt_y_2_value),
            .opt_x_1_rob(opt_x_1_rob), .opt_y_1_rob(opt_y_1_rob), .opt_x_2_rob(opt_x_2_rob), .opt_y_2_rob(opt_y_2_rob),
            .rs_1_ready(rs_1_ready), .rt_1_ready(rt_1_ready), .rs_2_ready(rs_2_ready), .rt_2_ready(rt_2_ready),
            .rs_1_value(rs_1_value), .rt_1_value(rt_1_value), .rs_2_value(rs_2_value), .rt_2_value(rt_2_value),
            .rs_1_rob(rs_1_rob), .rt_1_rob(rt_1_rob), .rs_2_rob(rs_2_rob), .rt_2_rob(rt_2_rob), .rd_1_rob(rd_1_rob), .rd_2_rob(rd_2_rob));
    
    OP_SCH op_sch1(.clk(clk), .rst(rst),
        .pc_1_in(pc_1_OP), .pc_2_in(pc_2_OP),
        .imm_1_in(imm_out_1_op), .imm_2_in(imm_out_2_op),
        .rs_1_ready_in(rs_1_ready), .rt_1_ready_in(rt_1_ready), .rs_2_ready_in(rs_2_ready), .rt_2_ready_in(rt_2_ready),
        .rs_1_value_in(rs_1_value), .rt_1_value_in(rt_1_value), .rs_2_value_in(rs_2_value), .rt_2_value_in(rt_2_value),
        .rs_1_rob_in(rs_1_rob), .rt_1_rob_in(rt_1_rob), .rs_2_rob_in(rs_2_rob), .rt_2_rob_in(rt_2_rob), .rd_1_rob_in(rd_1_rob), .rd_2_rob_in(rd_2_rob),
        .work_mode_1_in(work_mode_1_op), .work_mode_2_in(work_mode_2_op),
        .alu_ctrl_1_in(alu_ctrl_1_op), .alu_ctrl_2_in(alu_ctrl_2_op),
        .complex_ctrl_1_in(complex_ctrl_1_op), .complex_ctrl_2_in(complex_ctrl_2_op),
        .alu_commit_en_in(alu_done), .mul_commit_en_in(mul_done), .div_commit_en_in(div_done), .specific_commit_en_in(specific_done), .mem_commit_en_in(mem_done),
        .alu_commit_save_no_in(alu_commit_save_no), .mul_commit_save_no_in(mul_commit_save_no), .div_commit_save_no_in(div_commit_save_no), .specific_commit_save_no_in(specific_commit_save_no), .mem_commit_save_no_in(mem_commit_save_no),
        .alu_commit_rob_no_in(alu_commit_rob_no), .mul_commit_rob_no_in(mul_commit_rob_no), .div_commit_rob_no_in(div_commit_rob_no), .mem_commit_rob_no_in(mem_commit_rob_no),
        .alu_commit_value_in(alu_commit_value), .mul_commit_value_in(mul_commit_value), .div_commit_value_in(div_commit_value), .specific_commit_value_in(specific_commit_value), .mem_commit_value_in(mem_commit_value),
        .dm_rd_ctrl_1_in(dm_rd_ctrl_1_op), .dm_rd_ctrl_2_in(dm_rd_ctrl_2_op), .dm_wr_ctrl_1_in(dm_wr_ctrl_1_op), .dm_wr_ctrl_2_in(dm_wr_ctrl_2_op),
        .pc_1_out(pc_1_SCH), .pc_2_out(pc_2_SCH),
        .imm_1_out(imm_out_1_sch), .imm_2_out(imm_out_2_sch),
        .rs_1_ready_out(rs_1_ready_sch), .rt_1_ready_out(rt_1_ready_sch), .rs_2_ready_out(rs_2_ready_sch), .rt_2_ready_out(rt_2_ready_sch),
        .rs_1_value_out(rs_1_value_sch), .rt_1_value_out(rt_1_value_sch), .rs_2_value_out(rs_2_value_sch), .rt_2_value_out(rt_2_value_sch),
        .rs_1_rob_out(rs_1_rob_sch), .rt_1_rob_out(rt_1_rob_sch), .rs_2_rob_out(rs_2_rob_sch), .rt_2_rob_out(rt_2_rob_sch), .rd_1_rob_out(rd_1_rob_sch), .rd_2_rob_out(rd_2_rob_sch),
        .work_mode_1_out(work_mode_1_sch), .work_mode_2_out(work_mode_2_sch),
        .alu_ctrl_1_out(alu_ctrl_1_sch), .alu_ctrl_2_out(alu_ctrl_2_sch), .complex_ctrl_1_out(complex_ctrl_1_sch), .complex_ctrl_2_out(complex_ctrl_2_sch),
        .save_en_1(save_en_1), .save_en_2(save_en_2),
        .alu_commit_en_out(alu_done_sch), .mul_commit_en_out(mul_done_sch), .div_commit_en_out(div_done_sch), .specific_commit_en_out(specific_done_sch), .mem_commit_en_out(mem_done_sch),
        .alu_commit_save_no_out(alu_commit_save_no_sch), .mul_commit_save_no_out(mul_commit_save_no_sch), .div_commit_save_no_out(div_commit_save_no_sch), .specific_commit_save_no_out(specific_commit_save_no_sch), .mem_commit_save_no_out(mem_commit_save_no_sch),
        .alu_commit_rob_no_out(alu_commit_rob_no_sch), .mul_commit_rob_no_out(mul_commit_rob_no_sch), .div_commit_rob_no_out(div_commit_rob_no_sch), .mem_commit_rob_no_out(mem_commit_rob_no_sch),
        .alu_commit_value_out(alu_commit_value_sch), .mul_commit_value_out(mul_commit_value_sch), .div_commit_value_out(div_commit_value_sch), .specific_commit_value_out(specific_commit_value_sch), .mem_commit_value_out(mem_commit_value_sch),
        .dm_rd_ctrl_1_out(dm_rd_ctrl_1_sch), .dm_rd_ctrl_2_out(dm_rd_ctrl_2_sch), .dm_wr_ctrl_1_out(dm_wr_ctrl_1_sch), .dm_wr_ctrl_2_out(dm_wr_ctrl_2_sch));


    //实例化保留站
    saveStation s1(.clk(clk),
                   .save_en_1(save_en_1), .save_en_2(save_en_2), 
                   .pc_1(pc_1_SCH), .pc_2(pc_2_SCH),
                   .work_mode_1(work_mode_1_sch), .work_mode_2(work_mode_2_sch),
                   .alu_ctrl_1(alu_ctrl_1_sch), .alu_ctrl_2(alu_ctrl_2_sch), .complex_ctrl_1(complex_ctrl_1_sch), .complex_ctrl_2(complex_ctrl_2_sch),
                   .imm_1(imm_out_1_sch), .imm_2(imm_out_2_sch),
                   .rs_1_ready(rs_1_ready_sch), .rt_1_ready(rt_1_ready_sch), .rs_2_ready(rs_2_ready_sch), .rt_2_ready(rt_2_ready_sch),
                   .rs_1_relation(rs_1_rob_sch), .rt_1_relation(rt_1_rob_sch), .rd_1_relation(rd_1_rob_sch),
                   .rs_2_relation(rs_2_rob_sch), .rt_2_relation(rt_2_rob_sch), .rd_2_relation(rd_2_rob_sch),
                   .rs_1_value(rs_1_value_sch), .rt_1_value(rt_1_value_sch), .rs_2_value(rs_2_value_sch), .rt_2_value(rt_2_value_sch),
                   .dm_rd_ctrl_1(dm_rd_ctrl_1_sch), .dm_rd_ctrl_2(dm_rd_ctrl_2_sch), .dm_wr_ctrl_1(dm_wr_ctrl_1_sch), .dm_wr_ctrl_2(dm_wr_ctrl_2_sch),
                   .alu_commit_en(alu_done_sch), .mul_commit_en(mul_done_sch), .div_commit_en(div_done_sch), .specific_commit_en(specific_done_sch), .mem_commit_en(mem_done_sch),
                   .alu_commit_save_no(alu_commit_save_no_sch), .mul_commit_save_no(mul_commit_save_no_sch), .div_commit_save_no(div_commit_save_no_sch), .specific_commit_save_no(specific_commit_save_no_sch), .mem_commit_save_no(mem_commit_save_no_sch),
                   .alu_commit_rob_no(alu_commit_rob_no_sch), .mul_commit_rob_no(mul_commit_rob_no_sch), .div_commit_rob_no(div_commit_rob_no_sch), .mem_commit_rob_no(mem_commit_rob_no_sch),
                   .alu_commit_value(alu_commit_value_sch), .mul_commit_value(mul_commit_value_sch), .div_commit_value(div_commit_value_sch), .specific_commit_value(specific_commit_value_sch), .mem_commit_value(mem_commit_value_sch),
                   .alu_save_no(alu_save_no), .mul_save_no(mul_save_no), .div_save_no(div_save_no), .specific_save_no(specific_save_no),
                   .alu_rd_rob_no(alu_rd_rob_no), .mul_rd_rob_no(mul_rd_rob_no), .div_rd_rob_no(div_rd_rob_no),
                   .common_ctrl(common_ctrl), .alu_X(alu_X), .alu_Y(alu_Y), .alu_ready_en(alu_ready_en),
                   .specific_ctrl(specific_ctrl), .specific_X(specific_X), .specific_Y(specific_Y), .specific_ready_en(specific_ready_en),
                   .mul_ctrl(mul_ctrl), .mul_X(mul_X), .mul_Y(mul_Y), .mul_ready_en(mul_ready_en),
                   .div_ctrl(div_ctrl), .div_X(div_X), .div_Y(div_Y),  .div_ready_en(div_ready_en),
                   .read_en_mem(read_en_mem), .write_en_mem(write_en_mem), .dm_rd_ctrl_mem(dm_rd_ctrl_mem), .dm_wr_ctrl_mem(dm_wr_ctrl_mem), .dm_addr_mem(dm_addr_mem), .dm_din_mem(dm_din_mem), .save_no_mem(save_no_mem), .rd_rob_no_mem(rd_rob_no_mem));

    //乘法器实例化
    wallace mul1(.clk(clk), .rst(), .data_ready(mul_ready_en), .x(mul_X), .y(mul_Y), .ctrl(mul_ctrl), .save_no_in(mul_save_no), .rd_rob_in(mul_rd_rob_no), .done(mul_done), .save_no_out(mul_commit_save_no), .rd_rob_out(mul_commit_rob_no), .result(mul_commit_value));

    //普通运算器实例化
    commonAlu a1(.clk(clk), .rst(), .data_ready(alu_ready_en), .x(alu_X), .y(alu_Y), .ctrl(common_ctrl), .save_no_in(alu_save_no), .rd_rob_in(alu_rd_rob_no), .done(alu_done), .save_no_out(alu_commit_save_no), .rd_rob_out(alu_commit_rob_no), .result(alu_commit_value));

    //专属运算器实例化
    specificAlu a2(.clk(clk), .rst(), .data_ready(specific_ready_en), .x(specific_X), .y(specific_Y), .ctrl(specific_ctrl), .save_no_in(specific_save_no), .rd_rob_in(), .done(specific_done), .save_no_out(specific_commit_save_no), .rd_rob_out(), .result(specific_commit_value));
    
    //除法器实例化
    divider d1(.clk(clk), .rst(), .data_ready(div_ready_en), .x(div_X), .y(div_Y), .ctrl(div_ctrl), .save_no_in(div_save_no), .rd_rob_in(div_rd_rob_no), .done(div_done), .save_no_out(div_commit_save_no), .rd_rob_out(div_commit_rob_no), .result(div_commit_value));

    //实例化数据存储器
    dataMemory mem1(.clk(clk), .rst(), .read_en(read_en_mem), .write_en(write_en_mem), .dm_rd_ctrl(dm_rd_ctrl_mem), .dm_wr_ctrl(dm_wr_ctrl_mem), .dm_addr(dm_addr_mem), .dm_din(dm_din_mem), .save_no_in(save_no_mem), .rd_rob_in(rd_rob_no_mem), .done(mem_done), .save_no_out(mem_commit_save_no), .rd_rob_out(mem_commit_rob_no), .dm_dout(mem_commit_value));
endmodule