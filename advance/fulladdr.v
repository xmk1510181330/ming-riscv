/**
 ** 一位全加器 也就是3-2压缩器
**/

module fulladdr (
    input wire a,
    input wire b,
    input wire carry_in,
    output wire s,
    output wire carry_out
);
    
    //根据真值表使用纯逻辑电路实现全加器逻辑
    assign carry_out = (a&b)|(carry_in&(a|b));
    assign s = a^b^carry_in;
endmodule