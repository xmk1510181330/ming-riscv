# MingRisc

#### 介绍
一款支持五级流水的RISC-V SOC🚀

#### 环境配置
安装配置iverilog+vscode，可以参考如下博客
1. Windows: https://zhuanlan.zhihu.com/p/367612172
2. Mac:     https://zhuanlan.zhihu.com/p/291571592

#### 使用说明

1.  cd ming-risc
2.  cd neatRTL
3.  sh build.sh

#### 项目执行效果截图
![输入图片说明](img/2271703162842_.pic.jpg)

#### 参考资料
1.  Verilog快速入门： https://www.bilibili.com/video/BV1iv4y1F7Km/
2.  中科大Verilog OJ网站：https://verilogoj.ustc.edu.cn
3.  CSDN博客：https://blog.csdn.net/qq_45677520/article/details/122386632
4.  RISC-V指令手册：http://riscvbook.com/chinese/RISC-V-Reader-Chinese-v2p1.pdf
5.  开源项目：https://gitee.com/liangkangnan/tinyriscv
6.  书籍：计算机体系结构--胡伟武

#### 说明
1.  项目内容设计标准参考中科大OJ，控制信号与数据通路设计原则参考OJ第五页题目说明
2.  多发射、乱序执行和指令预测技术尚未实现

#### 项目功能清单
![输入图片说明](img/2281703162947_.pic.jpg)